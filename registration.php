<?php /** Copyright © Feedback Company. All rights reserved. */ ?>
<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'FeedbackCompany_Reviews', __DIR__);
