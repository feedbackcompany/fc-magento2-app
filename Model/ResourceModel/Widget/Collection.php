<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\ResourceModel\Widget;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use FeedbackCompany\Reviews\Model\Widget;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget as ResourceWidget;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'widget_id';

    /**
     * Init model
     */
    protected function _construct()
    {
        $this->_init(Widget::class, ResourceWidget::class);
    }
}
