<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use FeedbackCompany\Reviews\Model\System\Config\Source\ExtendedWidgetType;
use Magento\Framework\Model\ResourceModel\Db\Context;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DB\Adapter\AdapterInterface;

class Widget extends AbstractDb
{
    private $widgetTypes = [
        WidgetType::SMALL,
        WidgetType::SMALL_ONE,
        WidgetType::SMALL_TWO,
        WidgetType::BIG,
        WidgetType::BIG_ONE,
        WidgetType::BIG_TWO,
        WidgetType::BAR_TYPE,
        WidgetType::SUMMARY_TYPE,
        WidgetType::STICKY_TYPE,
        ExtendedWidgetType::POP_UP,
        ExtendedWidgetType::INLINE,
        ExtendedWidgetType::SIDEBAR
    ];

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        Context $context,
        Logger $logger,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->logger = $logger;
    }

    /**
     * Init model
     */
    protected function _construct()
    {
        $this->_init('feedback_review_widget_types', 'widget_id');
    }

    /**
     * @param $type
     * @param $storeId
     * @return string
     * @throws LocalizedException
     */
    public function getByWidgetType($type, $storeId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getMainTable(), 'widget_id')
            ->where('widget_type = :widget_type')
            ->where('store_id = :store_id');

        $bind = [
            ':widget_type' => (string)$type,
            ':store_id' => (int)$storeId
        ];

        return $connection->fetchOne($select, $bind);
    }

    /**
     * Insert widget types for new store
     *
     * @param array $storeIds
     * @return $this
     */
    public function insertWidgetTypesByStore($storeIds)
    {
        try {
            $data = [];
            /** @var AdapterInterface $connection */
            $connection = $this->getConnection();
            $connection->beginTransaction();

            foreach ($storeIds as $id) {
                foreach ($this->widgetTypes as $type) {
                    $data[] = [
                        'widget_type'   => $type,
                        'store_id'      => $id
                    ];
                }
            }

            if ($connection
                    ->isTableExists($this->getMainTable()) && $data) {
                $connection
                    ->insertMultiple($this->getMainTable(), $data);
            }
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            $this->logger->error($e->getMessage());
        }

        return $this;
    }

    /**
     * Delete widget types by store
     *
     * @param int $storeId
     * @return $this
     */
    public function deleteWidgetTypes($storeId)
    {
        try {
            if ($storeId) {
                /** @var AdapterInterface $connection */
                $connection = $this->getConnection();
                $connection->delete(
                    $this->getMainTable(),
                    ['store_id = (?)' => $storeId]
                );
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $this;
    }

    /**
     * Get widget uuid by widget type
     *
     * @param $type
     * @param $storeId
     * @return string|null
     */
    public function getWidgetUuid($type, $storeId)
    {
        try {
            $select = $this->getConnection()
                ->select()
                ->from(
                    ['t1' => $this->getMainTable()],
                    new \Zend_Db_Expr('COALESCE(t1.widget_uuid, t2.widget_uuid)')
                )
                ->joinLeft(
                    ['t2' => $this->getMainTable()],
                    't2.widget_type = t1.widget_type and t2.store_id = 0',
                    []
                )
                ->where('t1.widget_type = (?)', $type)
                ->where('t1.store_id = (?)', $storeId);

            return $this->getConnection()->fetchOne($select);
        } catch (\Exception $e) {
            $message = 'Get Widget Uuid Error: ' . $e->getMessage();
            $this->logger->error($message);
            return null;
        }
    }
}
