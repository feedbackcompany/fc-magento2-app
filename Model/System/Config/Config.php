<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config;

use Magento\Store\Model\ScopeInterface;
use Magento\Config\Model\ResourceModel\Config as SystemConfig;
use Magento\Framework\App\ScopeInterface as DefaultScope;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Store\Model\Store;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Type\Config as CacheConfig;
use Magento\Framework\App\Config\ReinitableConfigInterface;

class Config
{
    const IS_ACTIVE = 'feedback_reviews/feedback_reviews_general/is_active';
    const ACCESS_TOKEN_PASS = 'feedback_reviews/feedback_reviews_auth/access_token';
    const CLIENT_ID_PASS = 'feedback_reviews/feedback_reviews_auth/client_id';
    const CLIENT_SECRET_PATH = 'feedback_reviews/feedback_reviews_auth/client_secret';
    const EXTENDED_WIDGET_TYPE = 'feedback_reviews/feedback_reviews_product_widget/extended_widget_type';
    const EMBED_CODE_DEFAULT = 'feedback_reviews/feedback_reviews_embed/embed_default';
    const EMBED_CODE_PRODUCT = 'feedback_reviews/feedback_reviews_embed/embed_product';
    const SHOW_ON_PLP = 'feedback_reviews/feedback_reviews_product_widget/show_on_listing';
    const SHOW_ON_PDP = 'feedback_reviews/feedback_reviews_product_widget/show_on_pdp';
    const INVITATION_TRIGGER = 'feedback_reviews/feedback_reviews_invitations/invitation_trigger';
    const INVITATION_DELAY = 'feedback_reviews/feedback_reviews_invitations/invitation_delay';
    const INVITATION_DELAY_TYPE = 'feedback_reviews/feedback_reviews_invitations/invitation_delay_type';
    const REMINDER_DELAY = 'feedback_reviews/feedback_reviews_invitations/reminder_delay';

    /**
     * @var ReinitableConfigInterface
     */
    private $scopeConfig;

    /**
     * @var SystemConfig
     */
    private $systemConfig;

    /**
     * @var ResourceConnection
     */
    private $adapter;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;

    public function __construct(
        ReinitableConfigInterface $scopeConfig,
        SystemConfig $systemConfig,
        ResourceConnection $resourceConnection,
        Logger $logger,
        TypeListInterface $cacheTypeList
    ) {
        $this->scopeConfig      = $scopeConfig;
        $this->systemConfig     = $systemConfig;
        $this->adapter          = $resourceConnection;
        $this->logger           = $logger;
        $this->cacheTypeList    = $cacheTypeList;
    }

    /**
     * Is active module
     *
     * @param int $storeId
     * @return bool
     */
    public function isActive($storeId = 0)
    {
        return $this->scopeConfig->isSetFlag(
            self::IS_ACTIVE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get access token for necessary store from system config
     *
     * @param int $storeId
     * @return string
     */
    public function getAccessToken($storeId = 0)
    {
        $scopeData = $this->getStoreAndScope($storeId);

        $select = $this->getConnection()->select()
            ->from(['main' => $this->adapter->getTableName('core_config_data')], 'value')
            ->where('path = (?)', self::ACCESS_TOKEN_PASS)
            ->where('scope = (?)', $scopeData['scope'])
            ->where('scope_id = (?)', $scopeData['store']);

        return $this->getConnection()->fetchOne($select);
    }

    public function getAccessTokenByDefaultWay($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::ACCESS_TOKEN_PASS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Check access token
     *
     * @param int $storeId
     * @return bool
     */
    public function isAccessTokenExist($storeId = 0)
    {
        return $this->scopeConfig->isSetFlag(
            self::ACCESS_TOKEN_PASS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Show product widget on PLP
     *
     * @param int $storeId
     * @return bool
     */
    public function isShowOnPLP($storeId = 0)
    {
        return $this->scopeConfig->isSetFlag(
            self::SHOW_ON_PLP,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Show product widget on PDP
     *
     * @param int $storeId
     * @return bool
     */
    public function isShowOnPDP($storeId = 0)
    {
        return $this->scopeConfig->isSetFlag(
            self::SHOW_ON_PDP,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get client id
     *
     * @param int $storeId
     * @return mixed
     */
    public function getClientId($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::CLIENT_ID_PASS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get client secret
     *
     * @param int $storeId
     * @return mixed
     */
    public function getClientSecret($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::CLIENT_SECRET_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get extended widget type
     *
     * @param int $storeId
     * @return mixed
     */
    public function getExtendedWidgetType($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::EXTENDED_WIDGET_TYPE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get default embed code
     *
     * @return mixed
     */
    public function getDefaultEmbedCode()
    {
        return $this->scopeConfig->getValue(
            self::EMBED_CODE_DEFAULT,
            ScopeInterface::SCOPE_STORE,
            Store::DEFAULT_STORE_ID
        );
    }

    /**
     * Get product embed code
     *
     * @return mixed
     */
    public function getProductEmbedCode()
    {
        return $this->scopeConfig->getValue(
            self::EMBED_CODE_PRODUCT,
            ScopeInterface::SCOPE_STORE,
            Store::DEFAULT_STORE_ID
        );
    }

    /**
     * Get invitation trigger
     *
     * @param int $storeId
     * @return mixed
     */
    public function getInvitationTrigger($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::INVITATION_TRIGGER,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get invitation delay
     *
     * @param int $storeId
     * @return mixed
     */
    public function getInvitationDelay($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::INVITATION_DELAY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get invitation delay type
     *
     * @param int $storeId
     * @return mixed
     */
    public function getInvitationDelayType($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::INVITATION_DELAY_TYPE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get reminder delay
     *
     * @param int $storeId
     * @return mixed
     */
    public function getReminderDelay($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            self::REMINDER_DELAY,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get store and scope
     *
     * @param $storeId
     * @return array
     */
    private function getStoreAndScope($storeId)
    {
        $storeId = $storeId ? $storeId : 0;
        $scope = DefaultScope::SCOPE_DEFAULT;

        if ($storeId) {
            $scope = ScopeInterface::SCOPE_STORES;
        }

        return [
            'store' => $storeId,
            'scope' => $scope
        ];
    }

    /**
     * Save access token in system config
     *
     * @param $accessToken
     * @param $storeId
     */
    public function saveAccessToken($accessToken, $storeId)
    {
        if ($accessToken) {
            $params = $this->getStoreAndScope($storeId);

            $this->systemConfig->saveConfig(
                self::ACCESS_TOKEN_PASS,
                $accessToken,
                $params['scope'],
                $params['store']
            );
            $this->reInitSystemConfig();
        }
    }

    /**
     * Delete access token from system config
     *
     * @param $storeId
     */
    public function deleteAccessToken($storeId)
    {
        $params = $this->getStoreAndScope($storeId);
        $this->systemConfig->deleteConfig(
            self::ACCESS_TOKEN_PASS,
            $params['scope'],
            $params['store']
        );
        $this->reInitSystemConfig();
    }

    /**
     * Save embed code to system config
     *
     * @param $embedCode
     * @param string $type
     */
    public function saveEmbedCode($embedCode, $type = 'default')
    {
        if ($embedCode) {
            $path = self::EMBED_CODE_DEFAULT;
            if ($type == 'product') {
                $path = self::EMBED_CODE_PRODUCT;
            }

            $this->systemConfig->saveConfig(
                $path,
                $embedCode,
                DefaultScope::SCOPE_DEFAULT,
                0
            );
        }
    }

    /**
     * Re init system config
     */
    public function reInitSystemConfig()
    {
        $this->cacheTypeList->cleanType(CacheConfig::TYPE_IDENTIFIER);
        $this->scopeConfig->reinit();
    }

    /**
     * @return AdapterInterface
     */
    private function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->adapter->getConnection();
        }

        return $this->connection;
    }
}
