<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config;

class WidgetTypeParams
{
    /**
     * Widget types
     *
     * @var array
     */
    private $widgetTypes = [
        Source\WidgetType::SMALL,
        Source\WidgetType::SMALL_ONE,
        Source\WidgetType::SMALL_TWO,
        Source\WidgetType::BIG,
        Source\WidgetType::BIG_ONE,
        Source\WidgetType::BIG_TWO,
        Source\WidgetType::BAR_TYPE,
        Source\WidgetType::SUMMARY_TYPE,
        Source\WidgetType::STICKY_TYPE,
        Source\ExtendedWidgetType::POP_UP,
        Source\ExtendedWidgetType::INLINE,
        Source\ExtendedWidgetType::SIDEBAR
    ];

    /**
     * Prepare widget params for api call - registration
     *
     * @param string $type
     * @return array
     */
    public function getWidgetTypesParams($type = '')
    {
        $params = [];

        if (!$type) {
            foreach ($this->widgetTypes as $type) {
                $params[$type] = $this->getWidgetParamsByType($type);
            }
        } else {
            $params[$type] = $this->getWidgetParamsByType($type);
        }

        return $params;
    }

    /**
     * Prepare widget params for each widget based on widget type
     *
     * @param $type
     * @return array
     */
    private function getWidgetParamsByType($type)
    {
        $params = [];
        if ($type) {
            switch ($type) {
                case Source\WidgetType::BAR_TYPE:
                    $params = ['type' => Source\WidgetType::BAR_TYPE];
                    break;
                case Source\WidgetType::SUMMARY_TYPE:
                    $params = ['type' => Source\WidgetType::SUMMARY_TYPE];
                    break;
                case Source\WidgetType::SMALL:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'small',
                            'amount_of_reviews' => 0
                        ]
                    ];
                    break;
                case Source\WidgetType::SMALL_ONE:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'small',
                            'amount_of_reviews' => 1
                        ]
                    ];
                    break;
                case Source\WidgetType::SMALL_TWO:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'small',
                            'amount_of_reviews' => 2
                        ]
                    ];
                    break;
                case Source\WidgetType::BIG:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'big',
                            'amount_of_reviews' => 0
                        ]
                    ];
                    break;
                case Source\WidgetType::BIG_ONE:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'big',
                            'amount_of_reviews' => 1
                        ]
                    ];
                    break;
                case Source\WidgetType::BIG_TWO:
                    $params = [
                        'type' => 'main',
                        'options' => [
                            'size' => 'big',
                            'amount_of_reviews' => 2
                        ]
                    ];
                    break;
                case Source\WidgetType::STICKY_TYPE:
                    $params = ['type' => Source\WidgetType::STICKY_TYPE];
                    break;
                case Source\ExtendedWidgetType::POP_UP:
                    $params = [
                        'type' => Source\WidgetType::EXTENDED_TYPE,
                        'options' => ['display_type' => Source\ExtendedWidgetType::POP_UP]
                    ];
                    break;
                case Source\ExtendedWidgetType::INLINE:
                    $params = [
                        'type' => Source\WidgetType::EXTENDED_TYPE,
                        'options' => ['display_type' => Source\ExtendedWidgetType::INLINE]
                    ];
                    break;
                case Source\ExtendedWidgetType::SIDEBAR:
                    $params = [
                        'type' => Source\WidgetType::EXTENDED_TYPE,
                        'options' => ['display_type' => Source\ExtendedWidgetType::SIDEBAR]
                    ];
                    break;
            }
        }

        return $params;
    }
}
