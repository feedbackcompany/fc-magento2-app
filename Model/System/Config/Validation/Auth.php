<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Validation;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class Auth extends Value
{
    const MIN_ALLOW_LENGTH = 32;
    const MAX_ALLOW_LENGTH = 32;

    /**
     * Validate client id/secret before save
     *
     * @return $this|Value
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $fieldValue = $this->getValue();
        $fieldConfig = $this->getFieldConfig();
        $label = null;

        if ($fieldConfig && !empty($fieldConfig['label'])) {
            $label = $fieldConfig['label'];
        }

        if (!$fieldValue || !trim($fieldValue)) {
            throw new LocalizedException(
                __('"%fieldName" is required. Enter and try again.', ['fieldName' => $label])
            );
        }

        if (strlen($fieldValue) < self::MIN_ALLOW_LENGTH || strlen($fieldValue) > self::MAX_ALLOW_LENGTH) {
            throw new LocalizedException(
                __(
                    '"%fieldName" must not be less than "%min" and more than "%max" characters',
                    [
                        'fieldName' => $label,
                        'min' => self::MIN_ALLOW_LENGTH,
                        'max' => self::MAX_ALLOW_LENGTH
                    ]
                )
            );
        }

        return $this;
    }
}
