<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Validation;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class NotEmpty extends Value
{
    /**
     * Validate field data from system configuration
     * Required fields should not be empty
     *
     * @return Value|void
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $fieldValue = $this->getValue();
        $fieldConfig = $this->getFieldConfig();
        $label = null;

        if ($fieldConfig && !empty($fieldConfig['label'])) {
            $label = $fieldConfig['label'];
        }

        if (!$fieldValue || !trim($fieldValue)) {
            throw new LocalizedException(
                __('"%fieldName" is required. Enter and try again.', ['fieldName' => $label])
            );
        }

        return $this;
    }
}
