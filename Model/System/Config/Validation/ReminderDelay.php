<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Validation;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class ReminderDelay extends Value
{
    /**
     * Validate reminder delay value
     * Should be greater than 0
     *
     * @return $this|Value
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $fieldValue = (int)$this->getValue();
        $fieldConfig = $this->getFieldConfig();
        $label = null;

        if ($fieldConfig && !empty($fieldConfig['label'])) {
            $label = $fieldConfig['label'];
        }

        if (!isset($fieldValue)) {
            throw new LocalizedException(
                __('"%fieldName" is required. Enter and try again.', ['fieldName' => $label])
            );
        }

        if (!is_numeric($fieldValue) || strpos($fieldValue,'.')!==false) {
            throw new LocalizedException(
                __('"%fieldName" should be integer.', ['fieldName' => $label])
            );
        }

        if ($fieldValue < 0) {
            throw new LocalizedException(
                __(
                    '"%fieldName" must not be less than "%min".',
                    [
                        'fieldName' => $label,
                        'min' => 0
                    ]
                )
            );
        }

        return $this;
    }
}
