<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Validation;

use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class InvitationDelay extends Value
{
    const MIN_ALLOW_VALUE = 0;
    const MAX_ALLOW_VALUE = 99;

    /**
     * Validate invitation delay value
     * Should be from 0 to 99
     *
     * @return $this|Value
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $fieldValue = $this->getValue();
        $fieldConfig = $this->getFieldConfig();
        $label = null;

        if ($fieldConfig && !empty($fieldConfig['label'])) {
            $label = $fieldConfig['label'];
        }

        if (!isset($fieldValue)) {
            throw new LocalizedException(
                __('"%fieldName" is required. Enter and try again.', ['fieldName' => $label])
            );
        }

        if (!is_numeric($fieldValue) || strpos($fieldValue,'.')!==false) {
            throw new LocalizedException(
                __('"%fieldName" should be integer.', ['fieldName' => $label])
            );
        }

        if ($fieldValue < self::MIN_ALLOW_VALUE || $fieldValue > self::MAX_ALLOW_VALUE) {
            throw new LocalizedException(
                __(
                    '"%fieldName" must not be less than "%min" and more than "%max" characters',
                    [
                        'fieldName' => $label,
                        'min' => self::MIN_ALLOW_VALUE,
                        'max' => self::MAX_ALLOW_VALUE
                    ]
                )
            );
        }

        return $this;
    }
}
