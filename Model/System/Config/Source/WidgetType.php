<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class WidgetType implements OptionSourceInterface
{
    const SMALL = 'main_small';
    const SMALL_ONE = 'main_small_one';
    const SMALL_TWO = 'main_small_two';
    const BIG = 'main_big';
    const BIG_ONE = 'main_big_one';
    const BIG_TWO = 'main_big_two';
    const BAR_TYPE = 'bar';
    const SUMMARY_TYPE = 'product-summary';
    const EXTENDED_TYPE = 'product-extended';
    const STICKY_TYPE = 'sticky';

    /**
     *  Source model for field:
     *  - widget type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Score (small 200px)'), 'value' => self::SMALL],
            ['label' => __('Score (small 200px) + 1 review'), 'value' => self::SMALL_ONE],
            ['label' => __('Score (small 200px) + 2 reviews'), 'value' => self::SMALL_TWO],
            ['label' => __('Score (big 300px)'), 'value' => self::BIG],
            ['label' => __('Score (big 300px) + 1 review'), 'value' => self::BIG_ONE],
            ['label' => __('Score (big 300px) + 2 reviews'), 'value' => self::BIG_TWO]
        ];
    }
}
