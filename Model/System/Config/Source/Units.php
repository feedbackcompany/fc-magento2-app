<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Units implements OptionSourceInterface
{
    /**
     * Source model for fields:
     *  - invitation delay
     *  - reminder delay
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Minutes'), 'value' => 'minutes'],
            ['label' => __('Hours'), 'value' => 'hours'],
            ['label' => __('Days'), 'value' => 'days'],
            ['label' => __('Weekdays'), 'value' => 'weekdays']
        ];
    }
}
