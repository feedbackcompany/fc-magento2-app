<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\System\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ExtendedWidgetType implements OptionSourceInterface
{
    const INLINE = 'inline';
    const SIDEBAR = 'sidebar';
    const POP_UP = 'popup';

    /**
     *  Source model for field:
     *  - extended widget type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Inline'), 'value' => self::INLINE],
            ['label' => __('Sidebar'), 'value' => self::SIDEBAR],
            ['label' => __('Pop-up'), 'value' => self::POP_UP]
        ];
    }
}
