<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\Api;

use FeedbackCompany\Reviews\Model\Api\Data\AccessToken;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class Client
{
    const GET = 'get';
    const POST = 'post';

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var Json
     */
    private $jsonConverter;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var AccessToken
     */
    private $accessToken;

    /**
     * @var bool
     */
    private $isFirst = true;

    public function __construct(Curl $curl, Json $json, Logger $logger, Config $config, AccessToken $accessToken)
    {
        $this->curl             = $curl;
        $this->jsonConverter    = $json;
        $this->logger           = $logger;
        $this->feedbackConfig   = $config;
        $this->accessToken      = $accessToken;
    }

    /**
     * Send request
     * Result Processing
     *
     * @param string $method
     * @param string $endPoint
     * @param bool $isAccessTokenNeed
     * @param bool $timeout
     * @param int $storeId
     * @param bool $code
     * @param array $params
     * @return array|string
     * @throws \Exception
     */
    public function execute(
        $method,
        $endPoint,
        $isAccessTokenNeed = false,
        $timeout = false,
        $storeId = 0,
        $code = false,
        $params = []
    ) {
        try {
            $data = $this->sendRequest($method, $endPoint, $isAccessTokenNeed, $timeout, $storeId, $params);
            $status = (!empty($data['status'])) ? $data['status'] : 500;
            $responseData = (!empty($data['response'])) ? $data['response'] : [];

            if ($status == 401 && $isAccessTokenNeed && $this->isFirst) {
                $this->isFirst = false;
                if (!$this->accessToken->registerAccessToken($storeId)) {
                    throw new \Exception('Can\'t get new access token.');
                }

                $data = $this->sendRequest($method, $endPoint, $isAccessTokenNeed, $timeout, $storeId, $params);
                $status = (!empty($data['status'])) ? $data['status'] : 500;
                $responseData = (!empty($data['response'])) ? $data['response'] : [];

                if ($status && !in_array($status, range(200, 299))) {
                    $this->feedbackConfig->deleteAccessToken($storeId);
                }
            }

            if (!$code) {
                $response = $this->jsonConverter->unserialize($responseData);
            } else {
                $response = $responseData;
            }

            if (!in_array($status, range(200, 299))) {
                if (is_array($response) && !empty($response['error'])) {
                    if (!empty($response['description'])) {
                        throw new \Exception($response['description']);
                    }

                    throw new \Exception($response['error']);
                } else {
                    throw new \Exception('Unknown error.');
                }
            }

            return $response;
        } catch (\Exception $e) {
            $message = 'Client error: ' . $e->getMessage();
            $this->logger->error($message);
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Send request by using curl
     *
     * @param string $method
     * @param string $endPoint
     * @param bool $isAccessTokenNeed
     * @param bool $timeout
     * @param int $storeId
     * @param array $params
     * @return array
     */
    private function sendRequest(
        $method,
        $endPoint,
        $isAccessTokenNeed = false,
        $timeout = false,
        $storeId = 0,
        $params = []
    ) {
        if ($isAccessTokenNeed) {
            $accessToken = $this->feedbackConfig->getAccessTokenByDefaultWay($storeId);
            $this->curl->addHeader('Authorization', 'Bearer ' . $accessToken);
            $this->curl->addHeader('Content-Type', 'application/json');
        }

        if ($params) {
            $params = $this->jsonConverter->serialize($params);
        }

        if ($timeout) {
            $this->curl->setTimeout(3000);
        }

        switch ($method) {
            case self::GET:
                $this->curl->get($endPoint);
                break;
            case self::POST:
                $this->curl->post($endPoint, $params);
                break;
        }

        return ['status' => $this->curl->getStatus(), 'response' => $this->curl->getBody()];
    }
}
