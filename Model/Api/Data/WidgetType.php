<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\Client;
use FeedbackCompany\Reviews\Model\System\Config\WidgetTypeParams;
use FeedbackCompany\Reviews\Model\WidgetRepository;
use FeedbackCompany\Reviews\Logger\Logger;

class WidgetType
{
    const WIDGET_REGISTRATION_ENDPOINT = 'https://www.feedbackcompany.com/api/v2/widgets?platform=magento2';

    /**
     * @var WidgetTypeParams
     */
    private $widgetParams;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var WidgetRepository
     */
    private $widgetRepository;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var WidgetEmbedCodes
     */
    private $embedCodes;

    public function __construct(
        WidgetTypeParams $widgetTypeParams,
        Client $client,
        WidgetRepository $widgetRepository,
        Logger $logger,
        WidgetEmbedCodes $embedCodes
    ) {
        $this->widgetParams     = $widgetTypeParams;
        $this->client           = $client;
        $this->widgetRepository = $widgetRepository;
        $this->logger           = $logger;
        $this->embedCodes       = $embedCodes;
    }

    /**
     * Register widget type in Feedback Company system
     * Save widget uuid
     *
     * @param int $storeId
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    public function registerWidgets($storeId, $type = '')
    {
        try {
            $widgetsParams = $this->widgetParams->getWidgetTypesParams($type);

            if ($widgetsParams) {
                foreach ($widgetsParams as $key => $params) {
                    $response = $this->client->execute(
                        'post',
                        self::WIDGET_REGISTRATION_ENDPOINT,
                        true,
                        true,
                        $storeId,
                        false,
                        $params
                    );

                    if (!isset($response['widget']) || !isset($response['widget']['type'])
                        || empty($response['widget']['type'])) {
                        throw new \Exception(__('Missing widget type in response.'));
                    }

                    if (!isset($response['widget']['uuid']) || empty($response['widget']['uuid'])) {
                        throw new \Exception(__('Missing widget uuid in response.'));
                    }

                    $this->saveRegisterWidgetUuid($storeId, $response['widget']['uuid'], $key);
                }
            }

            $this->embedCodes->registerEmbedCodes();
            return true;
        } catch (\Exception $e) {
             $message = 'Widget Type error: ' . $e->getMessage();
             $this->logger->error($message);
             throw new \Exception($e->getMessage());
        }
    }

    /**
     * Save widget uuid
     *
     * @param int $storeId
     * @param string $uuid
     * @param string $type
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function saveRegisterWidgetUuid($storeId, $uuid, $type)
    {
        $widget = $this->widgetRepository->getByWidgetType($type, $storeId);
        if ($widget->getWidgetId()) {
            $widget->setWidgetUuid($uuid)
                ->save();
        }
    }
}
