<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\Api\Data;

use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Client;

class WidgetEmbedCodes
{
    // @codingStandardsIgnoreStart
    const EMBED_DEFAULT_ENDPOINT = 'https://www.feedbackcompany.com/includes/widgets/embed-code/v1/default.html?platform=magento2';
    const EMBED_PRODUCT_ENDPOINT = 'https://www.feedbackcompany.com/includes/widgets/embed-code/v1/product.html?platform=magento2';
    // @codingStandardsIgnoreEnd

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var Client
     */
    private $client;

    public function __construct(Config $config, Client $client)
    {
        $this->feedbackConfig   = $config;
        $this->client           = $client;
    }

    /**
     * Retrieve and save embed codes
     *
     * @throws \Exception
     */
    public function registerEmbedCodes()
    {
        $response = $this->client->execute(
            'get',
            self::EMBED_DEFAULT_ENDPOINT,
            false,
            true,
            0,
            true,
            []
        );

        if ($response) {
            $this->feedbackConfig->saveEmbedCode($response);
        }

        $response = $this->client->execute(
            'get',
            self::EMBED_PRODUCT_ENDPOINT,
            false,
            true,
            0,
            true,
            []
        );

        if ($response) {
            $this->feedbackConfig->saveEmbedCode($response, 'product');
        }
    }
}
