<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\ClientFactory;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\Exception\LocalizedException;
use Magento\Config\Model\ResourceModel\Config as SystemConfig;

class AccessToken
{
    // @codingStandardsIgnoreLine
    const TOKEN_ENDPOINT = 'https://www.feedbackcompany.com/api/v2/oauth2/token?client_id=%s&client_secret=%s&grant_type=authorization_code&platform=magento2';

    /**
     * API client
     *
     * @var ClientFactory
     */
    private $clientFactory;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var SystemConfig
     */
    private $systemConfig;

    public function __construct(ClientFactory $client, Config $config, SystemConfig $systemConfig)
    {
        $this->clientFactory    = $client;
        $this->feedbackConfig   = $config;
        $this->systemConfig     = $systemConfig;
    }

    /**
     * Retrieve and save access token
     *
     * @param int $storeId
     * @return bool
     * @throws LocalizedException
     * @throws \Exception
     */
    public function registerAccessToken($storeId)
    {
        try {
            if (!$this->getClientId($storeId) || !$this->getClientSecret($storeId)) {
                throw new LocalizedException(__('The fields client id and client secret is required.'));
            }

            $url = sprintf(self::TOKEN_ENDPOINT, $this->getClientId($storeId), $this->getClientSecret($storeId));
            $client = $this->clientFactory->create();
            $response = $client
                ->execute('get', $url, false, false, $storeId, false, []);

            if ($response) {
                if (!isset($response['access_token']) ||
                    (isset($response['access_token']) && empty($response['access_token']))) {
                    throw new LocalizedException(__('The combination of client id and client secret is invalid.'));
                }

                $this->saveAccessToken($response['access_token'], $storeId);
            }

            return true;
        } catch (LocalizedException $e) {
            throw new LocalizedException(__('"%1"', $e->getMessage()));
        } catch (\Exception $e) {
            throw new \Exception(__('The combination of client id and client secret is invalid.'));
        }
    }

    /**
     * Save access token to system config
     *
     * @param string $accessToken
     * @param int $storeId
     */
    private function saveAccessToken($accessToken, $storeId)
    {
        $this->feedbackConfig->saveAccessToken($accessToken, $storeId);
    }

    /**
     * Get client id
     *
     * @param int $storeId
     * @return mixed|string
     */
    private function getClientId($storeId)
    {
        if (!$this->clientId) {
            $this->clientId = $this->feedbackConfig->getClientId($storeId);
        }

        return $this->clientId;
    }

    /**
     * Get client secret
     *
     * @param int $storeId
     * @return mixed|string
     */
    private function getClientSecret($storeId)
    {
        if (!$this->clientSecret) {
            $this->clientSecret = $this->feedbackConfig->getClientSecret($storeId);
        }

        return $this->clientSecret;
    }
}
