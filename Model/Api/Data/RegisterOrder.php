<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model\Api\Data;

use Magento\Sales\Model\Order;
use Magento\Catalog\Helper\ImageFactory as CatalogImageHelperFactory;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Client;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Store\Model\App\Emulation;
use Magento\Framework\App\Area;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;

class RegisterOrder
{
    const REGISTER_ORDER_ENDPOINT = 'https://www.feedbackcompany.com/api/v2/orders?platform=magento2';
    const UNIT_MINUTES = 'minutes';
    const UNIT_HOURS = 'hours';

    /**
     * @var CatalogImageHelperFactory
     */
    private $catalogImageHelperFactory;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Emulation
     */
    private $appEmulation;

    public function __construct(
        CatalogImageHelperFactory $image,
        Config $config,
        Client $client,
        Logger $logger,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Emulation $appEmulation
    ) {
        $this->catalogImageHelperFactory = $image;
        $this->feedbackConfig            = $config;
        $this->client                    = $client;
        $this->logger                    = $logger;
        $this->productRepository         = $productRepository;
        $this->searchCriteriaBuilder     = $searchCriteriaBuilder;
        $this->appEmulation              = $appEmulation;
    }

    /**
     * Send request - order registration in feedback company
     *
     * @param Order $order
     * @return $this
     */
    public function registerOrder($order)
    {
        try {
            $params = $this->prepareOrderParams($order);
            if ($params) {
                $this->client->execute(
                    'post',
                    self::REGISTER_ORDER_ENDPOINT,
                    true,
                    false,
                    $order->getStoreId(),
                    false,
                    $params
                );
            }

            return $this;
        } catch (\Exception $e) {
            $message = 'Register Order error: ' . $e->getMessage() . PHP_EOL;
            $message .= 'Order increment id: ' . $order->getIncrementId();
            $this->logger->error($message);
        }
    }

    /**
     * Prepare order params for request
     *
     * @param Order $order
     * @return array
     */
    private function prepareOrderParams($order)
    {
        $params = [];
        $orderItems = [];
        $items = [];
        $storeId = $order->getStoreId();
        $usedGroupedProducts = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $productId = $item->getProductId();
            if ($item->getProductType()=='grouped'){
                $superProductConfig = $item->getProductOptionByCode('super_product_config');
                if (is_array($superProductConfig) && isset($superProductConfig['product_id'])){
                    $productId = $superProductConfig['product_id'];
                    if(in_array($productId,$usedGroupedProducts)){
                        continue;
                    }
                    $usedGroupedProducts[] = $productId;
                }
            }
            $items[] = $productId;

        }

        if ($items && $collection = $this->getProductCollection($items)) {
            if ($collection->getTotalCount() > 0) {
                foreach ($collection->getItems() as $product) {
                    $orderItems[] = [
                        'external_id' => $product->getSku(),
                        'name'        => $product->getName(),
                        'url'         => $product->getProductUrl(),
                        'image_url'   => $this->getProductImageUrl($product, $storeId)
                    ];
                }
            }
        }

        $customer = [
            'email'     => $order->getCustomerEmail(),
            'fullname'  => $order->getCustomerName()
        ];

        $unit = $this->feedbackConfig->getInvitationDelayType($storeId);
        $invitationDelay = (int)$this->feedbackConfig->getInvitationDelay($storeId);
        $reminderDelay = (int)$this->feedbackConfig->getReminderDelay($storeId);
        $invitation['delay'] = [
            'unit'      => $unit,
            'amount'    => $invitationDelay
        ];

        if ($reminderDelay && $reminderDelay > 0) {
            $amount = $this->getReminderAmount($unit, $reminderDelay, $invitationDelay);
            $invitation['reminder'] = [
                'unit'      => $unit,
                'amount'    => $amount
            ];
        }

        if ($orderItems) {
            $params = [
                'external_id'   => $order->getIncrementId(),
                'customer'      => $customer,
                'products'      => $orderItems,
                'invitation'    => $invitation
            ];
        }

        return $params;
    }

    /**
     * Get reminder amount
     *
     * @param $unit
     * @param $reminderDelay
     * @param $invitationDelay
     * @return int
     */
    private function getReminderAmount($unit, $reminderDelay, $invitationDelay)
    {
        $amount = $reminderDelay;
        if ($unit == self::UNIT_MINUTES) {
            $day = 24 * 60;
            if ($reminderDelay <= $day) {
                $amount = $day + $reminderDelay;
            }
        } elseif ($unit == self::UNIT_HOURS && $reminderDelay <= 24) {
            $amount = 24 + $reminderDelay;
        } elseif ($reminderDelay <= $invitationDelay) {
            $amount = $invitationDelay + $reminderDelay;
        }

        return $amount;
    }

    /**
     * Get product image url
     *
     * @param $product
     * @param $storeId
     * @return string
     */
    private function getProductImageUrl($product, $storeId)
    {
        $this->appEmulation->startEnvironmentEmulation($storeId, Area::AREA_FRONTEND, true);
        $imageUrl = $this->catalogImageHelperFactory->create()
            ->init($product, 'product_base_image')->getUrl();
        $this->appEmulation->stopEnvironmentEmulation();
        return $imageUrl;
    }

    /**
     * Get product collection
     *
     * @param $productIds
     * @return ProductSearchResultsInterface
     */
    private function getProductCollection($productIds)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('entity_id', $productIds, 'in')
            ->create();

        return $this->productRepository->getList($searchCriteria);
    }
}
