<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model;

use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Catalog\Helper\Image as CatalogImageHelper;
use FeedbackCompany\Reviews\Logger\Logger;

class WidgetFilter
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var CatalogImageHelper
     */
    private $catalogImageHelper;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Widget
     */
    private $widgetResource;

    public function __construct(
        StoreManagerInterface $storeManager,
        Config $feedbackConfig,
        CatalogImageHelper $catalogImageHelper,
        Logger $logger,
        Widget $widget
    ) {
        $this->storeManager       = $storeManager;
        $this->feedbackConfig     = $feedbackConfig;
        $this->catalogImageHelper = $catalogImageHelper;
        $this->logger             = $logger;
        $this->widgetResource     = $widget;
    }

    /**
     * Prepare default embed code before render
     *
     * @param $type
     * @return mixed|string|string[]
     */
    public function prepareDefaultCode($type)
    {
        try {
            $widgetCode = $this->feedbackConfig->getDefaultEmbedCode();
            $storeId = $this->storeManager->getStore()->getId();
            $randomPrefix = $this->getRandomPrefix();
            $widgetUuid = $this->widgetResource->getWidgetUuid($type, $storeId);

            if ($widgetUuid && $widgetCode) {
                $widgetCode = str_replace("{prefix}", $randomPrefix, $widgetCode);
                $widgetCode = str_replace("{uuid}", $widgetUuid, $widgetCode);
            }
        } catch (\Exception $e) {
            $message = 'Prepare default code error: ' . $e->getMessage();
            $this->logger->error($message);
            $widgetCode = '';
        }

        return $widgetCode;
    }

    /**
     * Prepare product embed code before render
     *
     * @param $type
     * @param $product
     * @return mixed|string|string[]
     */
    public function prepareProductCode($type, $product)
    {
        try {
            $widgetCode = '';
            $imageId = 'product_page_image_large';
            if ($product->getId()) {
                $storeId = $this->storeManager->getStore()->getId();
                $widgetCode = $this->feedbackConfig->getProductEmbedCode();

                if ($widgetCode) {
                    if ($type == WidgetType::EXTENDED_TYPE) {
                        $type = $this->feedbackConfig->getExtendedWidgetType($storeId);
                    }
                    $randomPrefix = $this->getRandomPrefix();
                    $widgetUuid = $this->widgetResource->getWidgetUuid($type, $storeId);

                    if ($widgetUuid) {
                        $widgetCode = str_replace("{prefix}", $randomPrefix, $widgetCode);
                        $widgetCode = str_replace("{uuid}", $widgetUuid, $widgetCode);
                        $widgetCode = str_replace("{product_external_id}", $product->getSku(), $widgetCode);
                        $widgetCode = str_replace("{product_name}", $product->getName(), $widgetCode);
                        $widgetCode = str_replace("{product_url}", $product->getProductUrl(), $widgetCode);
                        $widgetCode = str_replace(
                            "{product_image_url}",
                            $this->catalogImageHelper->init($product, $imageId)->getUrl(),
                            $widgetCode
                        );
                    }
                }
            }
        } catch (\Exception $e) {
            $message = 'Prepare product code error: ' . $e->getMessage();
            $this->logger->error($message);
            $widgetCode = '';
        }

        return $widgetCode;
    }

    /**
     * Return random prefix
     *
     * @return string
     */
    protected function getRandomPrefix()
    {
        return substr(uniqid('', true), -5);
    }
}
