<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model;

use FeedbackCompany\Reviews\Api\Data\WidgetInterface;
use FeedbackCompany\Reviews\Api\WidgetRepositoryInterface;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use FeedbackCompany\Reviews\Api\Data\WidgetInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget\CollectionFactory;
use FeedbackCompany\Reviews\Api\Data\WidgetSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use FeedbackCompany\Reviews\Api\Data\WidgetSearchResultInterfaceFactory;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class WidgetRepository implements WidgetRepositoryInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var WidgetFactory
     */
    private $widgetFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var WidgetInterfaceFactory
     */
    private $widgetDataFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var WidgetSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Widget
     */
    private $widgetResourceModel;

    public function __construct(
        EntityManager $entityManager,
        WidgetFactory $widgetFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        WidgetInterfaceFactory $widgetDataFactory,
        CollectionFactory $collectionFactory,
        WidgetSearchResultInterfaceFactory $widgetSearchResultInterfaceFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Widget $widget
    ) {
        $this->entityManager         = $entityManager;
        $this->widgetFactory         = $widgetFactory;
        $this->dataObjectHelper      = $dataObjectHelper;
        $this->dataObjectProcessor   = $dataObjectProcessor;
        $this->widgetDataFactory     = $widgetDataFactory;
        $this->collectionFactory     = $collectionFactory;
        $this->searchResultFactory   = $widgetSearchResultInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->widgetResourceModel   = $widget;
    }

    /**
     * Save widget uuid
     *
     * @param WidgetInterface $widget
     * @return WidgetInterface|mixed
     * @throws \Exception
     */
    public function save(WidgetInterface $widget)
    {
        /** @var \FeedbackCompany\Reviews\Model\Widget $widgetModel */
        $widgetModel = $this->widgetFactory->create();

        if ($widgetId = $widget->getWidgetId()) {
            $this->entityManager->load($widgetModel, $widgetId);
        }

        $widgetModel->setOrigData(null, $widgetModel->getData());
        $this->dataObjectHelper->populateWithArray(
            $widgetModel,
            $this->dataObjectProcessor->buildOutputDataArray($widget, WidgetInterface::class),
            WidgetInterface::class
        );
        $this->entityManager->save($widgetModel);
        $widget = $this->getWidgetDataObject($widgetModel);
        return $widget;
    }

    /**
     * Get widget uuids collection
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return WidgetSearchResultInterface|mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var WidgetSearchResultInterface $searchResults */
        $searchResults = $this->searchResultFactory->create()
            ->setSearchCriteria($searchCriteria);
        /** @var \FeedbackCompany\Reviews\Model\ResourceModel\Widget\Collection $collection */
        $collection = $this->widgetFactory->create()->getCollection();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {

                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];

            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        if ($sortOrders = $searchCriteria->getSortOrders()) {
            /** @var \Magento\Framework\Api\SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder($sortOrder->getField(), $sortOrder->getDirection());
            }
        }

        $collection
            ->setCurPage($searchCriteria->getCurrentPage())
            ->setPageSize($searchCriteria->getPageSize());

        $widgets = [];
        /** @var Widget $widgetModel */
        foreach ($collection as $widgetModel) {
            $widgets[] = $this->getWidgetDataObject($widgetModel);
        }
        $searchResults->setItems($widgets);
        return $searchResults;
    }

    /**
     * Get widget uuid by widget type
     *
     * @param string $widgetType
     * @param int $storeId
     * @return WidgetInterface|mixed
     * @throws NoSuchEntityException
     */
    public function getByWidgetType($widgetType, $storeId)
    {
        $widget = $this->widgetDataFactory->create();
        $widgetId = $this->widgetResourceModel->getByWidgetType($widgetType, $storeId);
        if (!$widgetId) {
            throw new NoSuchEntityException(__('The widget that was requested doesn\'t exist.'));
        }
        $this->entityManager->load($widget, $widgetId);
        return $widget;
    }

    /**
     * Prepare object
     *
     * @param $widgetModel
     * @return WidgetInterface
     */
    private function getWidgetDataObject($widgetModel)
    {
        /** @var WidgetInterface $widgetDataObject */
        $widgetDataObject = $this->widgetDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $widgetDataObject,
            $widgetModel->getData(),
            WidgetInterface::class
        );

        return $widgetDataObject;
    }
}
