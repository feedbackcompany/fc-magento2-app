<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Model;

use Magento\Framework\Model\AbstractModel;
use FeedbackCompany\Reviews\Api\Data\WidgetInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget as ResourceWidgetType;

class Widget extends AbstractModel implements WidgetInterface
{
    protected function _construct()
    {
        $this->_init(ResourceWidgetType::class);
    }

    /**
     * @param int $widgetId
     * @return $this
     */
    public function setWidgetId($widgetId)
    {
        $this->setData(self::WIDGET_ID, $widgetId);
        return $this;
    }

    /**
     * @param string $widgetType
     * @return $this
     */
    public function setWidgetType($widgetType)
    {
        $this->setData(self::WIDGET_TYPE, $widgetType);
        return $this;
    }

    /**
     * @param string $widgetUuid
     * @return $this
     */
    public function setWidgetUuid($widgetUuid)
    {
        $this->setData(self::WIDGET_UUID, $widgetUuid);
        return $this;
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->setData(self::STORE_ID, $storeId);
        return $this;
    }

    /**
     * @return int
     */
    public function getWidgetId()
    {
        return $this->getData(self::WIDGET_ID);
    }

    /**
     * @return string
     */
    public function getWidgetType()
    {
        return $this->getData(self::WIDGET_TYPE);
    }

    /**
     * @return string
     */
    public function getWidgetUuid()
    {
        return $this->getData(self::WIDGET_UUID);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }
}
