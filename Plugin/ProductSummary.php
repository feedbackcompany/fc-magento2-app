<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Plugin;

use Magento\Review\Block\Product\ReviewRenderer;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\App\RequestInterface;
use FeedbackCompany\Reviews\Block\Widget\Type\ProductWidget;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product;

class ProductSummary
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductWidget
     */
    private $productWidget;

    public function __construct(
        StoreManagerInterface $storeManager,
        Config $config,
        RequestInterface $request,
        ProductWidget $productWidget
    ) {
        $this->storeManager   = $storeManager;
        $this->feedbackConfig = $config;
        $this->request        = $request;
        $this->productWidget  = $productWidget;
    }

    /**
     * Render product summary widget
     *
     * @param ReviewRenderer $subject
     * @param string $result
     * @param Product $product
     * @return string
     * @throws NoSuchEntityException
     */
    public function afterGetReviewsSummaryHtml(ReviewRenderer $subject, $result, $product)
    {
        $storeId = $this->storeManager->getStore()->getId();

        if (!$this->feedbackConfig->isActive($storeId)) {
            return '';
        }

        $currentPage = $this->request->getFullActionName();
        if ($currentPage == 'catalog_category_view' && !$this->feedbackConfig->isShowOnPLP($storeId)) {
            return '';
        }
        if ($currentPage == 'catalog_product_view' && !$this->feedbackConfig->isShowOnPDP($storeId)) {
            return '';
        }

        return $this->productWidget->getReviewsSummaryHtml($product);
    }
}
