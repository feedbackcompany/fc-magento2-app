<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Plugin\Sales\Model\ResourceModel;

use Magento\Sales\Model\ResourceModel\Order as SalesOrderResource;
use Magento\Framework\Model\AbstractModel;
use Magento\Sales\Model\Order as SalesOrder;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\RegisterOrder;

class Order
{
    /**
     * @var SalesOrder
     */
    private $orderModel;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var RegisterOrder
     */
    private $registerOrder;

    public function __construct(Config $config, RegisterOrder $registerOrder)
    {
        $this->feedbackConfig = $config;
        $this->registerOrder = $registerOrder;
    }

    /**
     * Save order model if it is suitable for the conditions.
     *
     * @param SalesOrderResource $subject
     * @param AbstractModel $model
     */
    public function beforeSave(SalesOrderResource $subject, AbstractModel $model)
    {
        if ($model instanceof SalesOrder) {
            $this->orderModel = $model;
        }
    }

    /**
     * Register order in feedback company system
     * Depends on system config - invitation trigger
     *
     * @param SalesOrderResource $subject
     * @param $result
     * @return mixed
     */
    public function afterSave(SalesOrderResource $subject, $result)
    {
        if (!$this->orderModel) {
            return $result;
        }
        $storeId = $this->orderModel->getStoreId();
        if ($this->feedbackConfig->isActive($storeId)) {
            $invitationTrigger = $this->feedbackConfig->getInvitationTrigger($storeId);
            if ($invitationTrigger && $invitationTrigger == $this->orderModel->getStatus()) {
                $this->registerOrder->registerOrder($this->orderModel);
            }
        }

        return $result;
    }
}
