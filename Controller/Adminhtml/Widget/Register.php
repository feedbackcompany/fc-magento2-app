<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Controller\Adminhtml\Widget;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetType;

class Register extends Action
{
    /**
     * @var JsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var WidgetType
     */
    protected $_widgetType;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        WidgetType $widgetType
    ) {
        parent::__construct($context);
        $this->_jsonFactory = $jsonFactory;
        $this->_widgetType = $widgetType;
    }

    /**
     * Ajax call for register widgets in feedback company system
     */
    public function execute()
    {
        try {
            $responseData = [];
            if ($this->getRequest()->isAjax()) {
                $storeId = $this->getRequest()->getParam('store') ? $this->getRequest()->getParam('store') : 0;

                if ($this->_widgetType->registerWidgets($storeId)) {
                    $responseData = [
                        'status' => 'Successful'
                    ];
                }
            }
        } catch (\Exception $e) {
            $responseData = [
                'status' => 'Failed',
                'error_message' => __('Registration is failed. Please connect with Feedback Company.')
            ];
        }

        $response = $this->_jsonFactory->create();
        return $response->setData($responseData);
    }
}
