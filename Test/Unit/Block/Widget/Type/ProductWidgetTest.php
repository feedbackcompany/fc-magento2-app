<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Widget\Type;

use PHPUnit\Framework\TestCase;
use Magento\Framework\View\Element\Template\Context;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product;
use Magento\Framework\View\LayoutInterface;
use FeedbackCompany\Reviews\Block\Widget\AbstractWidget;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class ProductWidgetTest extends TestCase
{
    /**
     * @var TestableProductWidgetTest
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var WidgetFilter|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetFilterMock;

    /**
     * @var Registry|\PHPUnit_Framework_MockObject_MockObject
     */
    private $registryMock;

    /**
     * @var Product|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productMock;

    /**
     * @var LayoutInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layoutMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    protected function setUp() :void
    {
        $this->widgetFilterMock = $this->createMock(WidgetFilter::class);
        $this->registryMock = $this->createMock(Registry::class);
        $this->productMock = $this->createMock(Product::class);
        $this->layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->feedbackConfigMock = $this->createMock(Config::class);

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $objectManager->getObject(
            Context::class,
            [
                'layout'     => $this->layoutMock
            ]
        );

        $this->object = new TestableProductWidgetTest(
            $this->contextMock,
            $this->widgetFilterMock,
            $this->registryMock,
            $this->feedbackConfigMock,
            [
                'widget_type' => 'product-summary'
            ]
        );
        $this->object->html = 'Html of product review';
    }

    public function testGetProductWidgetCode()
    {
        $widgetCode = 'Some widget code';
        $this->registryMock->expects($this->once())
            ->method('registry')
            ->with('current_product')
            ->willReturn($this->productMock);
        $this->widgetFilterMock->expects($this->once())
            ->method('prepareProductCode')
            ->with('product-summary', $this->productMock)
            ->willReturn($widgetCode);

        $this->assertEquals($widgetCode, $this->object->getWidgetCode());
    }

    public function testGetReviewSummaryHtml()
    {
        $this->productMock->expects($this->once())
            ->method('getId')
            ->willReturn(700);

        $this->assertEquals(
            'Html of product review',
            $this->object->getReviewsSummaryHtml($this->productMock)
        );
    }

    public function testGetReviewSummaryHtmlMissingProduct()
    {
        $this->productMock->expects($this->once())
            ->method('getId')
            ->willReturn(false);

        $this->assertEmpty($this->object->getReviewsSummaryHtml($this->productMock));
    }

    public function testProductWidgetInstance()
    {
        $this->assertInstanceOf(AbstractWidget::class, $this->object);
    }

    public function testIsEnableReviewTab()
    {
        $this->layoutMock->expects($this->once())
            ->method('getBlock')
            ->with('feedback.reviews.tab')
            ->willReturn(true);

        $this->assertEquals(true, $this->object->isEnableReviewTab());
    }

    public function testIsEnableReviewTabNo()
    {
        $this->layoutMock->expects($this->once())
            ->method('getBlock')
            ->with('feedback.reviews.tab')
            ->willReturn(false);

        $this->assertEquals(false, $this->object->isEnableReviewTab());
    }

    public function testGetCacheLifetime()
    {
        $this->assertNull($this->object->getCacheLifetime());
    }
}
