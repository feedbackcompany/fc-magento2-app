<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Widget\Type;

use FeedbackCompany\Reviews\Block\Widget\Type\ProductWidget;

class TestableProductWidgetTest extends ProductWidget
{
    /**
     * @var string
     */
    public $html;

    public function toHtml()
    {
        return $this->html;
    }
}
