<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Widget\Type;

use FeedbackCompany\Reviews\Block\Widget\Type\MainWidget;
use FeedbackCompany\Reviews\Block\Widget\AbstractWidget;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class MainWidgetTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var MainWidget
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var WidgetFilter|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetFilterMock;

    /**
     * @var Registry|\PHPUnit_Framework_MockObject_MockObject
     */
    private $registryMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    protected function setUp() :void
    {
        $this->contextMock = $this->createMock(Context::class);
        $this->widgetFilterMock = $this->createMock(WidgetFilter::class);
        $this->registryMock = $this->createMock(Registry::class);
        $this->feedbackConfigMock = $this->createMock(Config::class);

        $this->object = new MainWidget(
            $this->contextMock,
            $this->widgetFilterMock,
            $this->registryMock,
            $this->feedbackConfigMock,
            [
                'widget_type' => 'main_small_one'
            ]
        );
    }

    public function testMainWidgetInstanseOf()
    {
        $this->assertInstanceOf(AbstractWidget::class, $this->object);
    }

    public function testGetWidgetCode()
    {
        $widgetType = 'main_small_one';
        $code = '<!-- Feedback Company Widget (start) -->';
        $this->widgetFilterMock->expects($this->once())
            ->method('prepareDefaultCode')
            ->with($widgetType)
            ->willReturn($code);

        $this->assertSame($code, $this->object->getWidgetCode());
    }

    public function testGetCacheLifetime()
    {
        $this->assertNull($this->object->getCacheLifetime());
    }
}
