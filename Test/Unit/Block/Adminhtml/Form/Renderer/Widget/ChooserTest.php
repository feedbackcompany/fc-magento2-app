<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Adminhtml\Form\Renderer\Widget;

use FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Widget\Chooser;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\LayoutInterface;
use FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Widget\WidgetTypeImage;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\Escaper;

class ChooserTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Chooser
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var Data|\PHPUnit_Framework_MockObject_MockObject
     */
    private $helperMock;

    /**
     * @var AbstractElement|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractElementMock;

    /**
     * @var LayoutInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layoutMock;

    /**
     * @var WidgetTypeImage|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetTypeImageMock;

    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mathRandomMock;

    /**
     * @var UrlInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    protected function setUp() :void
    {
        $this->helperMock = $this->createMock(Data::class);
        $this->abstractElementMock = $this->getMockBuilder(AbstractElement::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'getId',
                    'getValue',
                    'setData',
                ]
            )
            ->getMock();
        $this->layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mathRandomMock = $this->getMockBuilder(Random::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlBuilderMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->escaper = $this->getMockBuilder(Escaper::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'escapeHtml',
                ]
            )
            ->getMock();
        $this->widgetTypeImageMock = $this->getMockBuilder(WidgetTypeImage::class)
            ->disableOriginalConstructor()
            ->setMethods(['toHtml'])
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $objectManager->getObject(
            Context::class,
            [
                'layout'     => $this->layoutMock,
                'mathRandom' => $this->mathRandomMock,
                'urlBuilder' => $this->urlBuilderMock,
                'escaper'    => $this->escaper,
            ]
        );

        $this->object = new Chooser($this->contextMock, $this->helperMock, []);
    }

    public function testPrepareElementHtml()
    {
        $html = '<p>Some html</p>';
        $this->layoutMock->expects($this->once())
            ->method('createBlock')
            ->with(WidgetTypeImage::class)
            ->willReturn($this->widgetTypeImageMock);
        $this->widgetTypeImageMock->expects($this->atLeastOnce())
            ->method('toHtml')
            ->willReturn($html);
        $this->abstractElementMock->expects($this->once())
            ->method('setData')
            ->with('after_element_html', $html)
            ->willReturnSelf();

        $this->assertEquals($this->abstractElementMock, $this->object->prepareElementHtml($this->abstractElementMock));
    }
}
