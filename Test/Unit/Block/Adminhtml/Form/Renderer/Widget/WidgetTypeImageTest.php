<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Adminhtml\Form\Renderer\Widget;

use FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Widget\WidgetTypeImage;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Template;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\Escaper;

class WidgetTypeImageTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetTypeImage
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var Json|\PHPUnit_Framework_MockObject_MockObject
     */
    private $jsonMock;

    /**
     * @var LayoutInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layoutMock;

    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mathRandomMock;

    /**
     * @var UrlInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    protected function setUp() :void
    {
        $this->jsonMock = $this->createMock(Json::class);

        $this->layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mathRandomMock = $this->getMockBuilder(Random::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlBuilderMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->escaper = $this->getMockBuilder(Escaper::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'escapeHtml',
                ]
            )
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $objectManager->getObject(
            Context::class,
            [
                'layout'     => $this->layoutMock,
                'mathRandom' => $this->mathRandomMock,
                'urlBuilder' => $this->urlBuilderMock,
                'escaper'    => $this->escaper,
            ]
        );

        $this->object = new WidgetTypeImage(
            $this->contextMock,
            $this->jsonMock,
            []
        );
    }

    public function testGetWidgetConfig()
    {
        $expectedResult = '{"main_small":"first url","main_small_one":"second url"}';
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn($expectedResult);

        $this->assertEquals($expectedResult, $this->object->getWidgetConfig());
    }

    public function testWidgetTypeImageInstanceOf()
    {
        $this->assertInstanceOf(Template::class, $this->object);
    }
}
