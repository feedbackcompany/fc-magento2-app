<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Adminhtml\Form\Renderer\Config;

use Magento\Backend\Block\Template\Context;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\Escaper;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\App\RequestInterface;

class RegistrationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TestableRegistrationTest
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfig;

    /**
     * @var Json|\PHPUnit_Framework_MockObject_MockObject
     */
    private $jsonMock;

    /**
     * @var LayoutInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layoutMock;

    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mathRandomMock;

    /**
     * @var UrlInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    /**
     * @var Button|\PHPUnit_Framework_MockObject_MockObject
     */
    private $buttonMock;

    /**
     * @var AbstractElement|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractBlockMock;

    /**
     * @var Field|\PHPUnit_Framework_MockObject_MockObject
     */
    private $fieldMock;

    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    protected function setUp() :void
    {
        $this->feedbackConfig = $this->createMock(Config::class);
        $this->jsonMock = $this->createMock(Json::class);
        $this->layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mathRandomMock = $this->getMockBuilder(Random::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlBuilderMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->escaper = $this->getMockBuilder(Escaper::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'escapeHtml',
                ]
            )
            ->getMock();
        $this->buttonMock = $this->getMockBuilder(Button::class)
            ->disableOriginalConstructor()
            ->setMethods(['setData', 'toHtml'])
            ->getMock();
        $this->abstractBlockMock = $this->getMockBuilder(AbstractElement::class)
            ->disableOriginalConstructor()
            ->setMethods(['unsScope', 'unsCanUseWebsiteValue', 'unsCanUseDefaultValue', 'getHtmlId', 'getLabel'])
            ->getMockForAbstractClass();
        $this->fieldMock = $this->getMockBuilder(Field::class)
            ->disableOriginalConstructor()
            ->setMethods(['_getElementHtml', 'render'])
            ->getMock();
        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getParam'])
            ->getMockForAbstractClass();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $objectManager->getObject(
            Context::class,
            [
                'layout'     => $this->layoutMock,
                'mathRandom' => $this->mathRandomMock,
                'urlBuilder' => $this->urlBuilderMock,
                'escaper'    => $this->escaper,
                'request'    => $this->requestMock,
            ]
        );

        $this->object = new TestableRegistrationTest($this->contextMock, $this->jsonMock, $this->feedbackConfig, []);
        $this->object->elementHtml = 'test';
    }

    public function testRender()
    {
        // @codingStandardsIgnoreLine
        $html = '<tr id="row_test_HTML_id"><td class="label"><label for="test_HTML_id"><span>test_label</span></label></td><td class="value">test</td><td class=""></td></tr>';
        $htmlId = 'test_HTML_id';
        $label  = 'test_label';
        $this->abstractBlockMock
            ->expects($this->any())
            ->method('getHtmlId')
            ->willReturn($htmlId);
        $this->abstractBlockMock
            ->expects($this->any())
            ->method('getLabel')
            ->willReturn($label);
        $this->abstractBlockMock->expects($this->once())
            ->method('unsScope')
            ->willReturnSelf();
        $this->abstractBlockMock->expects($this->once())
            ->method('unsCanUseWebsiteValue')
            ->willReturnSelf();
        $this->abstractBlockMock->expects($this->once())
            ->method('unsCanUseDefaultValue')
            ->willReturnSelf();

        $this->assertEquals($html, $this->object->render($this->abstractBlockMock));
    }

    public function testGetAjaxUrl()
    {
        $url = "https:\/\/feedbackcompany.com\/feedback\/register\/widget";
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn($url);

        $this->assertEquals($url, $this->object->getAjaxUrl());
    }

    public function testGetButtonHtml()
    {
        $html = '<p>Button Html</p>';
        $storeId = 0;
        $this->requestMock->expects($this->once())
            ->method('getParam')
            ->with('store')
            ->willReturn($storeId);
        $this->feedbackConfig->expects($this->once())
            ->method('getAccessToken')
            ->with($storeId)
            ->willReturn('11112222');
        $this->layoutMock->expects($this->once())
            ->method('createBlock')
            ->with(Button::class)
            ->willReturn($this->buttonMock);
        $this->buttonMock->expects($this->once())
            ->method('setData')
            ->with(
                [
                'id'     => 'registration_button',
                'label'  => __('Register')
                ]
            )
            ->willReturnSelf();
        $this->buttonMock->expects($this->once())
            ->method('toHtml')
            ->willReturn($html);
        $expectedResult = $html . '<p class="note">
                        <span>'
            . __('Successfully connected! Press the button to register your widgets.')
            . '</span>
                     </p>';

        $this->assertEquals($expectedResult, $this->object->getButtonHtml());
    }

    public function testRegistrationInstanceOf()
    {
        $this->assertInstanceOf(Field::class, $this->object);
    }
}
