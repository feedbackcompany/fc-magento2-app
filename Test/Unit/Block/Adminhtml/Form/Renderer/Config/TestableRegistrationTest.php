<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Adminhtml\Form\Renderer\Config;

use FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Config\Registration;
use Magento\Framework\Data\Form\Element\AbstractElement;

class TestableRegistrationTest extends Registration
{
    /**
     * @var string
     */
    public $elementHtml;

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->elementHtml;
    }
}
