<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Block\Adminhtml\Form\Renderer\Config;

use FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Config\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\UrlInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class StatusTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Status
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var ScopeConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfigMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfig;

    /**
     * @var LayoutInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layoutMock;

    /**
     * @var Random|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mathRandomMock;

    /**
     * @var UrlInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $urlBuilderMock;

    /**
     * @var Escaper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $escaper;

    /**
     * @var AbstractElement|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractBlockMock;

    protected function setUp() :void
    {
        $this->feedbackConfig = $this->createMock(Config::class);
        $this->requestMock = $this->createMock(RequestInterface::class);
        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);
        $this->layoutMock = $this->getMockBuilder(LayoutInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mathRandomMock = $this->getMockBuilder(Random::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->urlBuilderMock = $this->getMockBuilder(UrlInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->escaper = $this->getMockBuilder(Escaper::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'escapeHtml',
                ]
            )
            ->getMock();

        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->contextMock = $objectManager->getObject(
            Context::class,
            [
                'layout'     => $this->layoutMock,
                'mathRandom' => $this->mathRandomMock,
                'urlBuilder' => $this->urlBuilderMock,
                'escaper'    => $this->escaper,
                'request'    => $this->requestMock,
            ]
        );

        $this->abstractBlockMock = $this->getMockBuilder(AbstractElement::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->object = new Status(
            $this->contextMock,
            $this->requestMock,
            $this->scopeConfigMock,
            $this->feedbackConfig,
            []
        );
    }

    public function testRenderValue()
    {
        $method = new \ReflectionMethod(Status::class, '_renderValue');
        $method->setAccessible(true);
        $this->requestMock->expects($this->atLeastOnce())
            ->method('getParam')
            ->with('store')
            ->willReturn(0);
        $this->feedbackConfig->expects($this->once())
            ->method('getAccessToken')
            ->with(0)
            ->willReturn('11112222');
        $expectedResult = '<td class="value">';
        $expectedResult .= '<div class="status-container"><span class="status-active" 
                        style="color: #00ff11">' . __('Active') . '</span></div>';
        $expectedResult .= '<p class="note">
                    <span>' . __('Status of selected store view.') .
            '</span></p>';
        $expectedResult .= '</td>';

        $this->assertEquals($expectedResult, $method->invoke($this->object, $this->abstractBlockMock));
    }

    public function testRenderValueInActive()
    {
        $method = new \ReflectionMethod(Status::class, '_renderValue');
        $method->setAccessible(true);
        $this->requestMock->expects($this->atLeastOnce())
            ->method('getParam')
            ->with('store')
            ->willReturn(0);
        $this->feedbackConfig->expects($this->once())
            ->method('getAccessToken')
            ->with(0)
            ->willReturn(null);
        $expectedResult = '<td class="value">';
        $expectedResult .= '<div class="status-container"><span class="status-inactive" 
                        style="color: #ff0000">' . __('Inactive') . '</span></div>';
        $expectedResult .= '<p class="note">
                    <span>' . __('Status of selected store view.') .
            '</span></p>';
        $expectedResult .= '</td>';

        $this->assertEquals($expectedResult, $method->invoke($this->object, $this->abstractBlockMock));
    }

    public function testStatusInstanceOf()
    {
        $this->assertInstanceOf(Field::class, $this->object);
    }
}
