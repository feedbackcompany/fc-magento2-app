<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model;

use FeedbackCompany\Reviews\Api\Data\WidgetInterface;
use FeedbackCompany\Reviews\Model\Widget;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;
use FeedbackCompany\Reviews\Api\Data\WidgetInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget\CollectionFactory;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget\Collection;
use FeedbackCompany\Reviews\Api\Data\WidgetSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use FeedbackCompany\Reviews\Api\Data\WidgetSearchResultInterfaceFactory;
use FeedbackCompany\Reviews\Model\WidgetFactory;
use FeedbackCompany\Reviews\Model\WidgetRepository;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\SortOrder;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget as WidgetResourceModel;

class WidgetRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var EntityManager|\PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var WidgetFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetFactory;

    /**
     * @var DataObjectHelper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor|\PHPUnit_Framework_MockObject_MockObject
     */
    private $dataObjectProcessor;

    /**
     * @var WidgetInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetDataFactory;

    /**
     * @var CollectionFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $collectionFactory;

    /**
     * @var WidgetSearchResultInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $searchResultFactory;

    /**
     * @var SearchCriteriaBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaBuilder;

    /**
     * @var WidgetResourceModel|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetResourceModel;

    /**
     * @var WidgetRepository
     */
    private $widgetRepository;

    private $widgetData = [
        'widget_id' => 1,
        'widget_type' => 'main_small',
        'widget_uuid' => 'bb725998-a427-41e1-adc6-4384c29bd872',
        'store_id' => 0
    ];

    protected function setUp() :void
    {
        $this->entityManager = $this->getMockBuilder(EntityManager::class)
            ->setMethods(['load', 'delete', 'save'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->widgetFactory = $this->getMockBuilder(WidgetFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->widgetDataFactory = $this->getMockBuilder(WidgetInterfaceFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->dataObjectHelper = $this->getMockBuilder(DataObjectHelper::class)
            ->setMethods(['populateWithArray'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->dataObjectProcessor = $this->getMockBuilder(DataObjectProcessor::class)
            ->setMethods(['buildOutputDataArray'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->collectionFactory = $this->createPartialMock(CollectionFactory::class, ['create']);
        $this->searchResultFactory = $this->createPartialMock(
            WidgetSearchResultInterfaceFactory::class,
            ['create']
        );
        $this->searchCriteriaBuilder = $this->getMockBuilder(SearchCriteriaBuilder::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $this->widgetResourceModel = $this->getMockBuilder(WidgetResourceModel::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();

        $this->widgetRepository = new WidgetRepository(
            $this->entityManager,
            $this->widgetFactory,
            $this->dataObjectHelper,
            $this->dataObjectProcessor,
            $this->widgetDataFactory,
            $this->collectionFactory,
            $this->searchResultFactory,
            $this->searchCriteriaBuilder,
            $this->widgetResourceModel
        );
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testSave()
    {
        $widgetModelMock = $this->getMockBuilder(Widget::class)
            ->setMethods(['setOrigData', 'getData'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->widgetFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetModelMock);
        $widgetMock = $this->getMockForAbstractClass(WidgetInterface::class);
        $widgetMock->expects($this->exactly(1))
            ->method('getWidgetId')
            ->willReturn($this->widgetData['widget_id']);
        $this->entityManager->expects($this->once())
            ->method('load')
            ->with($widgetModelMock, $this->widgetData['widget_id']);
        $widgetModelMock->expects($this->exactly(2))
            ->method('getData')
            ->willReturn($this->widgetData);
        $widgetModelMock->expects($this->once())
            ->method('setOrigData')
            ->with(null, $this->widgetData);

        $this->dataObjectProcessor->expects($this->once())
            ->method('buildOutputDataArray')
            ->with($widgetMock, WidgetInterface::class)
            ->willReturn($this->widgetData);
        $this->dataObjectHelper->expects($this->at(0))
            ->method('populateWithArray')
            ->with($widgetModelMock, $this->widgetData, WidgetInterface::class);
        $this->entityManager->expects($this->once())
            ->method('save')
            ->with($widgetModelMock);

        $this->widgetDataFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetMock);
        $widgetMockSecond = $this->getMockForAbstractClass(WidgetInterface::class);
        $this->widgetDataFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetMockSecond);
        $this->dataObjectHelper->expects($this->at(1))
            ->method('populateWithArray')
            ->with($widgetMockSecond, $this->widgetData, WidgetInterface::class);

        $this->assertSame($widgetMock, $this->widgetRepository->save($widgetMock));
    }

    public function testGetList()
    {
        $filterWidgetType = 'widget_type';
        $filterWidgetValue = 'main_small';
        $collectionSize = 3;
        $currPage = 1;
        $pageSize = 5;

        $searchCriteriaMock = $this->getMockForAbstractClass(
            SearchCriteriaInterface::class,
            [],
            '',
            false
        );
        $searchResultMock = $this->getMockForAbstractClass(
            WidgetSearchResultInterface::class,
            [],
            '',
            false
        );
        $searchResultMock->expects($this->once())
            ->method('setSearchCriteria')
            ->with($searchCriteriaMock)
            ->willReturnSelf();
        $this->searchResultFactory->expects($this->once())
            ->method('create')
            ->willReturn($searchResultMock);

        $collectionMock = $this->getMockBuilder(Collection::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $widgetModelMock = $this->getMockBuilder(Widget::class)
            ->setMethods(['getCollection', 'getData'])
            ->disableOriginalConstructor()
            ->getMock();
        $widgetModelMock->expects($this->once())
            ->method('getCollection')
            ->willReturn($collectionMock);
        $widgetModelMock->expects($this->once())
            ->method('getData')
            ->willReturn($this->widgetData);

        $this->widgetFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetModelMock);

        $filterGroupMock = $this->getMockBuilder(FilterGroup::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $filterMock = $this->getMockBuilder(Filter::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $searchCriteriaMock->expects($this->once())
            ->method('getFilterGroups')
            ->willReturn([$filterGroupMock]);
        $filterGroupMock->expects($this->once())
            ->method('getFilters')
            ->willReturn([$filterMock]);
        $filterMock->expects($this->once())
            ->method('getConditionType')
            ->willReturn(false);
        $filterMock->expects($this->once())
            ->method('getField')
            ->willReturn($filterWidgetType);
        $filterMock->expects($this->atLeastOnce())
            ->method('getValue')
            ->willReturn($filterWidgetValue);
        $collectionMock->expects($this->once())
            ->method('addFieldToFilter')
            ->with([$filterWidgetType], [['eq' => $filterWidgetValue]]);
        $collectionMock->expects($this->once())
            ->method('getSize')
            ->willReturn($collectionSize);
        $searchResultMock->expects($this->once())
            ->method('setTotalCount')
            ->with($collectionSize);

        $sortOrderMock = $this->getMockBuilder(SortOrder::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $searchCriteriaMock->expects($this->atLeastOnce())
            ->method('getSortOrders')
            ->willReturn([$sortOrderMock]);
        $sortOrderMock->expects($this->once())
            ->method('getField')
            ->willReturn($filterWidgetType);
        $collectionMock->expects($this->once())
            ->method('addOrder')
            ->with($filterWidgetType, SortOrder::SORT_ASC);
        $sortOrderMock->expects($this->once())
            ->method('getDirection')
            ->willReturn(SortOrder::SORT_ASC);
        $searchCriteriaMock->expects($this->once())
            ->method('getCurrentPage')
            ->willReturn($currPage);
        $collectionMock->expects($this->once())
            ->method('setCurPage')
            ->with($currPage)
            ->willReturn($collectionMock);
        $searchCriteriaMock->expects($this->once())
            ->method('getPageSize')
            ->willReturn($pageSize);
        $collectionMock->expects($this->once())
            ->method('setPageSize')
            ->with($pageSize)
            ->willReturn($collectionMock);
        $collectionMock->expects($this->once())
            ->method('getIterator')
            ->willReturn(new \ArrayIterator([$widgetModelMock]));

        $widgetMock = $this->getMockForAbstractClass(WidgetInterface::class);
        $this->widgetDataFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetMock);
        $this->dataObjectHelper->expects($this->once())
            ->method('populateWithArray')
            ->with($widgetMock, $this->widgetData, WidgetInterface::class);

        $searchResultMock->expects($this->once())
            ->method('setItems')
            ->with([$widgetMock])
            ->willReturnSelf();

        $this->assertSame($searchResultMock, $this->widgetRepository->getList($searchCriteriaMock));
    }

    public function testGetByWidgetType()
    {
        $widgetId = 29;
        $widgetType = 'main_big_one';
        $storeId = 2;

        $widgetMock = $this->getMockForAbstractClass(WidgetInterface::class);
        $this->widgetDataFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetMock);
        $this->widgetResourceModel->expects($this->once())
            ->method('getByWidgetType')
            ->with($widgetType, $storeId)
            ->willReturn($widgetId);
        $this->entityManager->expects($this->once())
            ->method('load')
            ->with($widgetMock, $widgetId)
            ->willReturn($widgetMock);
        $this->assertSame($widgetMock, $this->widgetRepository->getByWidgetType($widgetType, $storeId));
    }

    public function testGetAbsentWidget()
    {
        $this->expectExceptionMessage('The widget that was requested doesn\'t exist.');
        $widgetMock = $this->getMockForAbstractClass(WidgetInterface::class);
        $this->widgetDataFactory->expects($this->once())
            ->method('create')
            ->willReturn($widgetMock);
        $this->widgetResourceModel->expects($this->once())
            ->method('getByWidgetType')
            ->with('test_widget_type', 0)
            ->will($this->returnValue(null));
        $this->widgetRepository->getByWidgetType('test_widget_type', 0);
    }
}
