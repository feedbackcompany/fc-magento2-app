<?php
// @codingStandardsIgnoreFile
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model;

use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Catalog\Helper\Image as CatalogImageHelper;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Catalog\Model\Product;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class WidgetFilterTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var CatalogImageHelper|\PHPUnit_Framework_MockObject_MockObject
     */
    private $catalogImageHelperMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var StoreInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeMock;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $productMock;

    /**
     * @var Widget|\PHPUnit\Framework\MockObject\MockObject
     */
    private $widgetResourceMock;

    /**
     * @var TestableWidgetFilter
     */
    private $object;

    protected function setUp() :void
    {
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->catalogImageHelperMock = $this->getMockBuilder(CatalogImageHelper::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->loggerMock = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->productMock = $this->createMock(Product::class);
        $this->widgetResourceMock = $this->createMock(Widget::class);

        $this->object = new TestableWidgetFilter(
            $this->storeManagerMock,
            $this->feedbackConfigMock,
            $this->catalogImageHelperMock,
            $this->loggerMock,
            $this->widgetResourceMock
        );
        $this->object->randomPrefix = '9510';
    }

    public function testPrepareDefaultCode()
    {
        $this->feedbackConfigMock->expects($this->once())
            ->method('getDefaultEmbedCode')
            ->willReturn('<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" 
                                    id="__fbcw__{prefix}{uuid}">
                                <!-- Feedback Company Widget (end) -->');
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $this->widgetResourceMock->expects($this->once())
            ->method('getWidgetUuid')
            ->willReturn('90b3fe80-fc82-4836-9d1e-056b722ec558');
        $expectedResult = '<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" 
                                    id="__fbcw__951090b3fe80-fc82-4836-9d1e-056b722ec558">
                                <!-- Feedback Company Widget (end) -->';
        $this->assertSame($expectedResult, $this->object->prepareDefaultCode('main_small'));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testPrepareDefaultCodeException()
    {
        $this->feedbackConfigMock->expects($this->once())
            ->method('getDefaultEmbedCode')
            ->willReturn('<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" 
                                    id="__fbcw__{prefix}{uuid}">
                                <!-- Feedback Company Widget (end) -->');
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->willThrowException(
                new \Magento\Framework\Exception\NoSuchEntityException(
                    __("The store that was requested wasn't found. Verify the store and try again.")
                )
            );
        $message = 'Prepare default code error: The store that was requested wasn\'t found. Verify the store and try again.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);
        $this->assertEmpty($this->object->prepareDefaultCode('main_small'));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testPrepareProductCode()
    {
        $imageId = 'product_page_image_large';
        $this->productMock->expects($this->once())
            ->method('getId')
            ->willReturn(10);
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $this->feedbackConfigMock->expects($this->once())
            ->method('getProductEmbedCode')
            ->willReturn('<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" id="__fbcw__{prefix}{uuid}">
                                        "use strict";!function(){
                                    window.FeedbackCompanyWidgets=window.FeedbackCompanyWidgets||{queue:[],loaders:[
                                    ]};var options={uuid:"{uuid}",version:"1.2.1",prefix:"{prefix}",
                                    urlParams:{product_external_id:"{product_external_id}"},
                                    templateParams:{product_name:"{product_name}",product_url:"{product_url}",
                                    product_image_url:"{product_image_url}"}};if(
                                    void 0===window.FeedbackCompanyWidget){if(
                                    window.FeedbackCompanyWidgets.queue.push(options),!document.getElementById(
                                    "__fbcw_FeedbackCompanyWidget")){var scriptTag=document.createElement("script")
                                    ;scriptTag.onload=function(){if(window.FeedbackCompanyWidget)for(
                                    ;0<window.FeedbackCompanyWidgets.queue.length;
                                    )options=window.FeedbackCompanyWidgets.queue.pop(),
                                    window.FeedbackCompanyWidgets.loaders.push(
                                    new window.FeedbackCompanyWidgetLoader(options))},
                                    scriptTag.id="__fbcw_FeedbackCompanyWidget",
                                    scriptTag.src="https://www.feedbackcompany.com/includes/widgets/feedback-company-widget.min.js"
                                    ,document.body.appendChild(scriptTag)}
                                    }else window.FeedbackCompanyWidgets.loaders.push(
                                    new window.FeedbackCompanyWidgetLoader(options))}();
                                    </script>
                                    <!-- Feedback Company Widget (end) -->
                                    ');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getExtendedWidgetType')
            ->with(1)
            ->willReturn('inline');
        $this->widgetResourceMock->expects($this->once())
            ->method('getWidgetUuid')
            ->with('inline', 1)
            ->willReturn('7b4e73b4-5142-477f-8e5e-bbd431eda124');
        $this->productMock->expects($this->once())
            ->method('getSku')
            ->willReturn('MT04');
        $this->productMock->expects($this->once())
            ->method('getName')
            ->willReturn('Black Bag');
        $this->productMock->expects($this->once())
            ->method('getProductUrl')
            ->willReturn('https://feedbackcompany.com/black-bag.html');
        $this->catalogImageHelperMock->expects($this->once())
            ->method('init')
            ->with($this->productMock, $imageId)
            ->willReturnSelf();
        $this->catalogImageHelperMock->expects($this->once())
            ->method('getUrl')
            ->willReturn('http://feedbackcompany.com/pub/media/catalog/product/cache/80a38b87accc339476be07e68cc86bd7/w/b/mt04-black-0.jpg');
        $expectedResutl = '<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" id="__fbcw__95107b4e73b4-5142-477f-8e5e-bbd431eda124">
                                        "use strict";!function(){
                                    window.FeedbackCompanyWidgets=window.FeedbackCompanyWidgets||{queue:[],loaders:[
                                    ]};var options={uuid:"7b4e73b4-5142-477f-8e5e-bbd431eda124",version:"1.2.1",prefix:"9510",
                                    urlParams:{product_external_id:"MT04"},
                                    templateParams:{product_name:"Black Bag",product_url:"https://feedbackcompany.com/black-bag.html",
                                    product_image_url:"http://feedbackcompany.com/pub/media/catalog/product/cache/80a38b87accc339476be07e68cc86bd7/w/b/mt04-black-0.jpg"}};if(
                                    void 0===window.FeedbackCompanyWidget){if(
                                    window.FeedbackCompanyWidgets.queue.push(options),!document.getElementById(
                                    "__fbcw_FeedbackCompanyWidget")){var scriptTag=document.createElement("script")
                                    ;scriptTag.onload=function(){if(window.FeedbackCompanyWidget)for(
                                    ;0<window.FeedbackCompanyWidgets.queue.length;
                                    )options=window.FeedbackCompanyWidgets.queue.pop(),
                                    window.FeedbackCompanyWidgets.loaders.push(
                                    new window.FeedbackCompanyWidgetLoader(options))},
                                    scriptTag.id="__fbcw_FeedbackCompanyWidget",
                                    scriptTag.src="https://www.feedbackcompany.com/includes/widgets/feedback-company-widget.min.js"
                                    ,document.body.appendChild(scriptTag)}
                                    }else window.FeedbackCompanyWidgets.loaders.push(
                                    new window.FeedbackCompanyWidgetLoader(options))}();
                                    </script>
                                    <!-- Feedback Company Widget (end) -->
                                    ';
        $this->assertSame($expectedResutl, $this->object->prepareProductCode('product-extended', $this->productMock));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testPrepareProductCodeException()
    {
        $this->productMock->expects($this->once())
            ->method('getId')
            ->willReturn(10);
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->willThrowException(
                new \Magento\Framework\Exception\NoSuchEntityException(
                    __("The store that was requested wasn't found. Verify the store and try again.")
                )
            );
        $message = 'Prepare product code error: The store that was requested wasn\'t found. Verify the store and try again.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);
        $this->assertEmpty($this->object->prepareProductCode('product-extended', $this->productMock));
    }
}
