<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model;

use Magento\Framework\Model\AbstractModel;
use FeedbackCompany\Reviews\Api\Data\WidgetInterface;
use FeedbackCompany\Reviews\Model\Widget;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

class WidgetTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Widget
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var Registry|\PHPUnit_Framework_MockObject_MockObject
     */
    private $registryMock;

    /**
     * @var AbstractResource|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractResourceMock;

    /**
     * @var AbstractDb|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractDbMock;

    /**
     * @var AbstractModel|\PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractModelMock;

    protected function setUp() :void
    {
        $this->contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->registryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->abstractResourceMock = $this->getMockBuilder(AbstractResource::class)
            ->setMethods(['getIdFieldName'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->abstractDbMock = $this->getMockBuilder(AbstractDb::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->abstractModelMock = $this->getMockBuilder(AbstractModel::class)
            ->setMethods(['setData', 'getData'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->object = new Widget(
            $this->contextMock,
            $this->registryMock,
            $this->abstractResourceMock,
            $this->abstractDbMock,
            []
        );
    }

    public function testWidgetClass()
    {
        $this->assertInstanceOf(AbstractModel::class, $this->object);
    }

    public function testWidgetInterface()
    {
        $this->assertInstanceOf(WidgetInterface::class, $this->object);
    }

    public function testSetWidgetId()
    {
        $this->object->setWidgetId(1);
        $this->assertEquals(1, $this->object->getWidgetId());
    }

    public function testSetWidgetType()
    {
        $this->object->setWidgetType('main_small');
        $this->assertEquals('main_small', $this->object->getWidgetType());
    }

    public function testSetWidgetUuid()
    {
        $this->object->setWidgetUuid('90b3fe80-fc82-4836-9d1e-056b722ec558');
        $this->assertEquals('90b3fe80-fc82-4836-9d1e-056b722ec558', $this->object->getWidgetUuid());
    }

    public function testSetStoreId()
    {
        $this->object->setStoreId(2);
        $this->assertEquals(2, $this->object->getStoreId());
    }

    public function testGetWidgetId()
    {
        $this->object->setWidgetId(1);
        $this->assertEquals(1, $this->object->getWidgetId());
    }

    public function testGetWidgetType()
    {
        $this->object->setWidgetType('big_one');
        $this->assertEquals('big_one', $this->object->getWidgetType());
    }

    public function testGetWidgetUuid()
    {
        $this->object->setWidgetUuid('90b3fe80-fc82-4836-9d1e-056b722ec558');
        $this->assertEquals('90b3fe80-fc82-4836-9d1e-056b722ec558', $this->object->getWidgetUuid());
    }

    public function testGetStoreId()
    {
        $this->object->setStoreId(2);
        $this->assertEquals(2, $this->object->getStoreId());
    }
}
