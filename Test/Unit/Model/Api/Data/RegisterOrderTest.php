<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\Data\RegisterOrder;
use Magento\Catalog\Helper\Image as CatalogImageHelper;
use Magento\Catalog\Helper\ImageFactory as CatalogImageHelperFactory;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Client;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Framework\App\Area;

class RegisterOrderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var RegisterOrder
     */
    private $object;

    /**
     * @var CatalogImageHelperFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $catalogImageHelperFactoryMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var Client|\PHPUnit_Framework_MockObject_MockObject
     */
    private $clientMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var ProductRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productRepositoryMock;

    /**
     * @var SearchCriteriaBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaBuilderMock;

    /**
     * @var Order|\PHPUnit_Framework_MockObject_MockObject
     */
    private $orderMock;

    /**
     * @var OrderItemInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItemMock;

    /**
     * @var SearchCriteria|\PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaMock;

    /**
     * @var ProductSearchResultsInterface
     */
    private $searchResultMock;

    /**
     * @var ProductInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productMock;

    /**
     * @var Emulation|\PHPUnit_Framework_MockObject_MockObject
     */
    private $emulationMock;

    protected function setUp() :void
    {
        $this->catalogImageHelperFactoryMock = $this->getMockBuilder(CatalogImageHelperFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->feedbackConfigMock = $this->createMock(Config::class);
        $this->clientMock = $this->createMock(Client::class);
        $this->loggerMock = $this->createMock(Logger::class);
        $this->productRepositoryMock = $this->createMock(ProductRepositoryInterface::class);
        $this->searchCriteriaBuilderMock = $this->getMockBuilder(SearchCriteriaBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(['addFilter', 'create'])
            ->getMock();
        $this->orderMock = $this->createMock(Order::class);
        $this->orderItemMock = $this->createMock(OrderItemInterface::class);
        $this->searchCriteriaMock = $this->createMock(SearchCriteria::class);
        $this->searchResultMock = $this->getMockBuilder(ProductSearchResultsInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->productMock = $this->getMockBuilder(ProductInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['getProductUrl'])
            ->getMockForAbstractClass();
        $this->emulationMock = $this->createMock(Emulation::class);

        $this->object = new RegisterOrder(
            $this->catalogImageHelperFactoryMock,
            $this->feedbackConfigMock,
            $this->clientMock,
            $this->loggerMock,
            $this->productRepositoryMock,
            $this->searchCriteriaBuilderMock,
            $this->emulationMock
        );
    }

    public function testRegisterOrder()
    {
        $catalogImageHelperMock = $this->createMock(CatalogImageHelper::class);
        $storeId = 1;
        $params = [
            'external_id' => '000000005',
            'customer' => [
                'email' => 'testperson@gmail.com',
                'fullname' => 'Test Person'
            ],
            'products' => [
                [
                    'external_id' => 'MT07',
                    'name' => 'Argus All-Weather Tank',
                    'url' => 'https://feedbackcompany.com/argus-all-weather-tank.html',
                    // @codingStandardsIgnoreLine
                    'image_url' => 'https://feedbackcompany.com/pub/static/adminhtml/Magento/backend/en_US/Magento_Catalog/images/product/placeholder.jpg'
                ]
            ],
            'invitation' => [
                'delay' => [
                    'unit' => 'days',
                    'amount' => 2
                ],
                'reminder' => [
                    'unit' => 'days',
                    'amount' => 3
                ]
            ]
        ];
        $response = [
            'success' => true
        ];
        $items = [
            'items' => $this->orderItemMock
        ];
        $productItems = [
            'items' => $this->productMock
        ];
        $this->orderMock->expects($this->once())
            ->method('getAllVisibleItems')
            ->willReturn($items);
        $this->orderItemMock->expects($this->once())
            ->method('getProductId')
            ->willReturn(700);
        $this->searchCriteriaBuilderMock->expects($this->once())
            ->method('addFilter')
            ->with('entity_id', [0 => 700], 'in')
            ->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->once())
            ->method('create')
            ->willReturn($this->searchCriteriaMock);
        $this->productRepositoryMock->expects($this->once())
            ->method('getList')
            ->with($this->searchCriteriaMock)
            ->willReturn($this->searchResultMock);
        $this->searchResultMock->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(1);
        $this->searchResultMock->expects($this->once())
            ->method('getItems')
            ->willReturn($productItems);
        $this->productMock->expects($this->once())
            ->method('getSku')
            ->willReturn('MT07');
        $this->productMock->expects($this->once())
            ->method('getName')
            ->willReturn('Argus All-Weather Tank');
        $this->productMock->expects($this->once())
            ->method('getProductUrl')
            ->willReturn('https://feedbackcompany.com/argus-all-weather-tank.html');
        $this->emulationMock->expects($this->once())
            ->method('startEnvironmentEmulation')
            ->with('1', Area::AREA_FRONTEND, true);
        $this->catalogImageHelperFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($catalogImageHelperMock);
        $catalogImageHelperMock->expects($this->once())
            ->method('init')
            ->with($this->productMock, 'product_base_image')
            ->willReturnSelf();
        $catalogImageHelperMock->expects($this->once())
            ->method('getUrl')
            // @codingStandardsIgnoreLine
            ->willReturn('https://feedbackcompany.com/pub/static/adminhtml/Magento/backend/en_US/Magento_Catalog/images/product/placeholder.jpg');
        $this->emulationMock->expects($this->once())
            ->method('stopEnvironmentEmulation')
            ->willReturnSelf();
        $this->orderMock->expects($this->once())
            ->method('getCustomerEmail')
            ->willReturn('testperson@gmail.com');
        $this->orderMock->expects($this->once())
            ->method('getCustomerName')
            ->willReturn('Test Person');
        $this->orderMock->expects($this->exactly(2))
            ->method('getStoreId')
            ->willReturnOnConsecutiveCalls($storeId, $storeId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getInvitationDelayType')
            ->withConsecutive(
                [$storeId],
                [$storeId]
            )
            ->willReturnOnConsecutiveCalls('days', 'days');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getInvitationDelay')
            ->with($storeId)
            ->willReturn(2);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getReminderDelay')
            ->with($storeId)
            ->willReturn(3);
        $this->orderMock->expects($this->atLeast(1))
            ->method('getIncrementId')
            ->willReturn('000000005');
        $this->clientMock->expects($this->once())
            ->method('execute')
            ->with(
                'post',
                'https://www.feedbackcompany.com/api/v2/orders?platform=magento2',
                true,
                false,
                $storeId,
                false,
                $params
            )
            ->willReturn($response);

        $this->assertInstanceOf(RegisterOrder::class, $this->object->registerOrder($this->orderMock));
    }

    public function testRegisterOrderException()
    {
        $catalogImageHelperMock = $this->createMock(CatalogImageHelper::class);
        $storeId = 1;
        $params = [
            'external_id' => '000000005',
            'customer' => [
                'email' => 'testperson@gmail.com',
                'fullname' => 'Test Person'
            ],
            'products' => [
                [
                    'external_id' => 'MT07',
                    'name' => 'Argus All-Weather Tank',
                    'url' => 'https://feedbackcompany.com/argus-all-weather-tank.html',
                    // @codingStandardsIgnoreLine
                    'image_url' => 'https://feedbackcompany.com/pub/static/adminhtml/Magento/backend/en_US/Magento_Catalog/images/product/placeholder.jpg'
                ]
            ],
            'invitation' => [
                'delay' => [
                    'unit' => 'days',
                    'amount' => 2
                ],
                'reminder' => [
                    'unit' => 'days',
                    'amount' => 3
                ]
            ]
        ];
        $items = [
            'items' => $this->orderItemMock
        ];
        $productItems = [
            'items' => $this->productMock
        ];
        $this->orderMock->expects($this->once())
            ->method('getAllVisibleItems')
            ->willReturn($items);
        $this->orderItemMock->expects($this->once())
            ->method('getProductId')
            ->willReturn(700);
        $this->searchCriteriaBuilderMock->expects($this->once())
            ->method('addFilter')
            ->with('entity_id', [0 => 700], 'in')
            ->willReturnSelf();
        $this->searchCriteriaBuilderMock->expects($this->once())
            ->method('create')
            ->willReturn($this->searchCriteriaMock);
        $this->productRepositoryMock->expects($this->once())
            ->method('getList')
            ->with($this->searchCriteriaMock)
            ->willReturn($this->searchResultMock);
        $this->searchResultMock->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(1);
        $this->searchResultMock->expects($this->once())
            ->method('getItems')
            ->willReturn($productItems);
        $this->productMock->expects($this->once())
            ->method('getSku')
            ->willReturn('MT07');
        $this->productMock->expects($this->once())
            ->method('getName')
            ->willReturn('Argus All-Weather Tank');
        $this->productMock->expects($this->once())
            ->method('getProductUrl')
            ->willReturn('https://feedbackcompany.com/argus-all-weather-tank.html');
        $this->emulationMock->expects($this->once())
            ->method('startEnvironmentEmulation')
            ->with('1', Area::AREA_FRONTEND, true);
        $this->catalogImageHelperFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($catalogImageHelperMock);
        $catalogImageHelperMock->expects($this->once())
            ->method('init')
            ->with($this->productMock, 'product_base_image')
            ->willReturnSelf();
        $catalogImageHelperMock->expects($this->once())
            ->method('getUrl')
            // @codingStandardsIgnoreLine
            ->willReturn('https://feedbackcompany.com/pub/static/adminhtml/Magento/backend/en_US/Magento_Catalog/images/product/placeholder.jpg');
        $this->emulationMock->expects($this->once())
            ->method('stopEnvironmentEmulation')
            ->willReturnSelf();
        $this->orderMock->expects($this->once())
            ->method('getCustomerEmail')
            ->willReturn('testperson@gmail.com');
        $this->orderMock->expects($this->once())
            ->method('getCustomerName')
            ->willReturn('Test Person');
        $this->orderMock->expects($this->exactly(2))
            ->method('getStoreId')
            ->willReturnOnConsecutiveCalls($storeId, $storeId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getInvitationDelayType')
            ->withConsecutive(
                [$storeId],
                [$storeId]
            )
            ->willReturnOnConsecutiveCalls('days', 'days');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getInvitationDelay')
            ->with($storeId)
            ->willReturn(2);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getReminderDelay')
            ->with($storeId)
            ->willReturn(3);
        $this->orderMock->expects($this->atLeast(1))
            ->method('getIncrementId')
            ->willReturn('000000005');
        $this->clientMock->expects($this->once())
            ->method('execute')
            ->with(
                'post',
                'https://www.feedbackcompany.com/api/v2/orders?platform=magento2',
                true,
                false,
                $storeId,
                false,
                $params
            )
            ->willThrowException(
                new \Exception('Can\'t get new access token.')
            );
        $message = 'Register Order error: Can\'t get new access token.' . PHP_EOL;
        $message .= 'Order increment id: 000000005';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->assertEmpty($this->object->registerOrder($this->orderMock));
    }
}
