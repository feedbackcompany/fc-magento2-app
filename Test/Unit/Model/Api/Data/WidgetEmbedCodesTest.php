<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\Data\WidgetEmbedCodes;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Client;

class WidgetEmbedCodesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetEmbedCodes
     */
    private $object;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var Client|\PHPUnit_Framework_MockObject_MockObject
     */
    private $clientMock;

    protected function setUp() :void
    {
        $this->feedbackConfigMock = $this->createMock(Config::class);
        $this->clientMock = $this->createMock(Client::class);

        $this->object = new WidgetEmbedCodes($this->feedbackConfigMock, $this->clientMock);
    }

    public function testRegisterEmbedCodes()
    {
        $defaultCode = 'default code';
        $productCode = 'product code';

        $this->clientMock->expects($this->at(0))
            ->method('execute')
            ->willReturn($defaultCode);
        $this->feedbackConfigMock->expects($this->at(0))
            ->method('saveEmbedCode')
            ->with($defaultCode)
            ->willReturn(null);
        $this->clientMock->expects($this->at(1))
            ->method('execute')
            ->willReturn($productCode);
        $this->feedbackConfigMock->expects($this->at(1))
            ->method('saveEmbedCode')
            ->with($productCode, 'product')
            ->willReturn(null);

        $this->assertNull($this->object->registerEmbedCodes());
    }
}
