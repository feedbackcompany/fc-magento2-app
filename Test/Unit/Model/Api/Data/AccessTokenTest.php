<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\Data\AccessToken;
use FeedbackCompany\Reviews\Model\Api\ClientFactory;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Config\Model\ResourceModel\Config as SystemConfig;
use FeedbackCompany\Reviews\Model\Api\Client;

class AccessTokenTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var AccessToken
     */
    private $object;

    /**
     * @var ClientFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $clientFactoryMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var SystemConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $systemConfigMock;

    /**
     * @var Client|\PHPUnit_Framework_MockObject_MockObject
     */
    private $clientMock;

    protected function setUp() :void
    {
        $this->clientFactoryMock = $this->getMockBuilder(ClientFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->feedbackConfigMock = $this->createMock(Config::class);
        $this->systemConfigMock = $this->createMock(SystemConfig::class);
        $this->clientMock = $this->createMock(Client::class);

        $this->object = new AccessToken($this->clientFactoryMock, $this->feedbackConfigMock, $this->systemConfigMock);
    }

    public function testRegisterAccessToken()
    {
        $clientId = '11112222';
        $clientSecret = '33334444';
        $storeId = 1;
        // @codingStandardsIgnoreLine
        $url = 'https://www.feedbackcompany.com/api/v2/oauth2/token?client_id=11112222&client_secret=33334444&grant_type=authorization_code&platform=magento2';
        $response = [
            'error' => false,
            'access_token' => '9999-8888'
        ];

        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientId')
            ->with($storeId)
            ->willReturn($clientId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientSecret')
            ->with($storeId)
            ->willReturn($clientSecret);
        $this->clientFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->clientMock);
        $this->clientMock->expects($this->once())
            ->method('execute')
            ->with('get', $url, false, false, $storeId, false, [])
            ->willReturn($response);

        $this->assertEquals(true, $this->object->registerAccessToken($storeId));
    }

    public function testRegisterAccessTokenLocalizedException()
    {
        $clientId = '11112222';
        $clientSecret = '33334444';
        $storeId = 1;
        // @codingStandardsIgnoreLine
        $url = 'https://www.feedbackcompany.com/api/v2/oauth2/token?client_id=11112222&client_secret=33334444&grant_type=authorization_code&platform=magento2';
        $response = [
            'error' => true,
            'description' => 'The combination of client id and client secret is invalid.'
        ];

        $this->expectExceptionMessage('The combination of client id and client secret is invalid.');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientId')
            ->with($storeId)
            ->willReturn($clientId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientSecret')
            ->with($storeId)
            ->willReturn($clientSecret);
        $this->clientFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->clientMock);
        $this->clientMock->expects($this->once())
            ->method('execute')
            ->with('get', $url, false, false, $storeId, false, [])
            ->willReturn($response);
        $this->object->registerAccessToken($storeId);
    }

    public function testRegisterAccessTokenMissingClientSecret()
    {
        $clientId = '11112222';
        $storeId = 1;

        $this->expectExceptionMessage('The fields client id and client secret is required.');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientId')
            ->with($storeId)
            ->willReturn($clientId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientSecret')
            ->with($storeId)
            ->willReturn(null);
        $this->object->registerAccessToken($storeId);
    }

    public function testRegisterAccessTokenUnexpectedError()
    {
        $clientId = '11112222';
        $clientSecret = '33334444';
        $storeId = 1;
        // @codingStandardsIgnoreLine
        $url = 'https://www.feedbackcompany.com/api/v2/oauth2/token?client_id=11112222&client_secret=33334444&grant_type=authorization_code&platform=magento2';

        $this->expectExceptionMessage('The combination of client id and client secret is invalid.');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientId')
            ->with($storeId)
            ->willReturn($clientId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getClientSecret')
            ->with($storeId)
            ->willReturn($clientSecret);
        $this->clientFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->clientMock);
        $this->clientMock->expects($this->once())
            ->method('execute')
            ->with('get', $url, false, false, $storeId, false, [])
            ->willThrowException(
                new \Exception('Unexpected error.')
            );
        $this->object->registerAccessToken($storeId);
    }
}
