<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\Api\Data;

use FeedbackCompany\Reviews\Model\Api\Data\WidgetType;
use FeedbackCompany\Reviews\Model\Api\Client;
use FeedbackCompany\Reviews\Model\System\Config\WidgetTypeParams;
use FeedbackCompany\Reviews\Model\WidgetRepository;
use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetEmbedCodes;
use FeedbackCompany\Reviews\Model\Widget;

class WidgetTypeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetType
     */
    private $object;

    /**
     * @var Client|\PHPUnit_Framework_MockObject_MockObject
     */
    private $clientMock;

    /**
     * @var WidgetTypeParams|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetTypeParamsMock;

    /**
     * @var WidgetRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetRepositoryMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var WidgetEmbedCodes|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetEmbedCodesMock;

    /**
     * @var Widget|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetMock;

    protected function setUp() :void
    {
        $this->clientMock = $this->createMock(Client::class);
        $this->widgetTypeParamsMock = $this->createMock(WidgetTypeParams::class);
        $this->widgetRepositoryMock = $this->createMock(WidgetRepository::class);
        $this->loggerMock = $this->createMock(Logger::class);
        $this->widgetEmbedCodesMock = $this->createMock(WidgetEmbedCodes::class);
        $this->widgetMock = $this->createMock(Widget::class);

        $this->object = new WidgetType(
            $this->widgetTypeParamsMock,
            $this->clientMock,
            $this->widgetRepositoryMock,
            $this->loggerMock,
            $this->widgetEmbedCodesMock
        );
    }

    public function testRegisterWidgets()
    {
        $storeId = 1;
        $widgetParams = [
            'bar' => ['type' => 'bar'],
            'product-summary' => ['type' => 'product-summary'],
            'main_small' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 0
                ]
            ],
            'main_small_one' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 1
                ]
            ],
            'main_small_two' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 2
                ]
            ],
            'main_big' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 0
                ]
            ],
            'main_big_one' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 1
                ]
            ],
            'main_big_two' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 2
                ]
            ],
            'sticky' => ['type' => 'sticky'],
            'popup' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'popup'
                ]
            ],
            'inline' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'inline'
                ]
            ],
            'sidebar' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'sidebar'
                ]
            ]
        ];
        $response = [
            'error' => false,
            'widget' => [
                'type' => 'small_one',
                'uuid' => 'u1985-7534-95120'
            ]
        ];

        $this->widgetTypeParamsMock->expects($this->once())
            ->method('getWidgetTypesParams')
            ->willReturn($widgetParams);
        $this->clientMock->expects($this->any())
            ->method('execute')
            ->willReturn($response);
        $this->widgetRepositoryMock->expects($this->any())
            ->method('getByWidgetType')
            ->willReturn($this->widgetMock);
        $this->widgetMock->expects($this->any())
            ->method('getWidgetId')
            ->willReturn(5);
        $this->widgetMock->expects($this->any())
            ->method('setWidgetUuid')
            ->willReturnSelf();
        $this->widgetMock->expects($this->any())
            ->method('save')
            ->willReturnSelf();
        $this->widgetEmbedCodesMock->expects($this->once())
            ->method('registerEmbedCodes')
            ->willReturn(null);

        $this->assertEquals(true, $this->object->registerWidgets($storeId));
    }

    public function testRegisterWidgetsException()
    {
        $storeId = 1;
        $widgetParams = [
            'bar' => ['type' => 'bar'],
            'product-summary' => ['type' => 'product-summary']
        ];
        $response = [
            'error' => false,
            'description' => 'Some error'
        ];

        $this->expectExceptionMessage('Missing widget type in response.');
        $this->widgetTypeParamsMock->expects($this->once())
            ->method('getWidgetTypesParams')
            ->willReturn($widgetParams);
        $this->clientMock->expects($this->any())
            ->method('execute')
            ->willReturn($response);
        $message = 'Widget Type error: Missing widget type in response.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->registerWidgets($storeId);
    }

    public function testRegisterWidgetsExceptionUuid()
    {
        $storeId = 1;
        $widgetParams = [
            'bar' => ['type' => 'bar'],
            'product-summary' => ['type' => 'product-summary']
        ];
        $response = [
            'error' => false,
            'widget' => [
                'type' => 'small_one'
            ]
        ];

        $this->expectExceptionMessage('Missing widget uuid in response.');
        $this->widgetTypeParamsMock->expects($this->once())
            ->method('getWidgetTypesParams')
            ->willReturn($widgetParams);
        $this->clientMock->expects($this->any())
            ->method('execute')
            ->willReturn($response);
        $message = 'Widget Type error: Missing widget uuid in response.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->registerWidgets($storeId);
    }
}
