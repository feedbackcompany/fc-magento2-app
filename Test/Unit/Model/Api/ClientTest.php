<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\Api;

use FeedbackCompany\Reviews\Model\Api\Client;
use FeedbackCompany\Reviews\Model\Api\Data\AccessToken;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class ClientTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Client
     */
    private $object;

    /**
     * @var AccessToken|\PHPUnit_Framework_MockObject_MockObject
     */
    private $accessTokenMock;

    /**
     * @var Curl|\PHPUnit_Framework_MockObject_MockObject
     */
    private $curlMock;

    /**
     * @var Json|\PHPUnit_Framework_MockObject_MockObject
     */
    private $jsonMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    protected function setUp() :void
    {
        $this->accessTokenMock = $this->createMock(AccessToken::class);
        $this->curlMock = $this->getMockBuilder(Curl::class)
            ->setMethods(['addHeader', 'setTimeout', 'get', 'post', 'getStatus', 'getBody'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->jsonMock = $this->createMock(Json::class);
        $this->loggerMock = $this->createMock(Logger::class);
        $this->feedbackConfigMock = $this->createMock(Config::class);

        $this->object = new Client(
            $this->curlMock,
            $this->jsonMock,
            $this->loggerMock,
            $this->feedbackConfigMock,
            $this->accessTokenMock
        );
    }

    public function testExecuteWithOutAccessToken()
    {
        $method = 'get';
        $endPoint = 'https://www.feedbackcompany.com/includes/widgets/embed-code/v1/default.html';
        $storeId = 1;
        $this->curlMock->expects($this->once())
            ->method('setTimeout')
            ->with(3000)
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method($method)
            ->with($endPoint)
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('getStatus')
            ->willReturn(200);
        $this->curlMock->expects($this->once())
            ->method('getBody')
            ->willReturn('<p>bar</p>');
        $this->assertEquals('<p>bar</p>', $this->object->execute(
            $method,
            $endPoint,
            false,
            true,
            $storeId,
            true,
            []
        ));
    }

    public function testExecuteGetToken()
    {
        $method = 'get';
        // @codingStandardsIgnoreLine
        $endPoint = 'https://www.feedbackcompany.com/api/v2/oauth2/token?client_id=%s&client_secret=%s&grant_type=authorization_code';
        $storeId = 0;
        $response = '{"error":false,"access_token":"5555-9999"}';
        $responseData = [
            'error' => false,
            'access_token' => '5555-9999'
        ];
        $this->curlMock->expects($this->once())
            ->method($method)
            ->with($endPoint)
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('getStatus')
            ->willReturn(200);
        $this->curlMock->expects($this->once())
            ->method('getBody')
            ->willReturn($response);
        $this->jsonMock->expects($this->once())
            ->method('unserialize')
            ->with($response)
            ->willReturn($responseData);

        $this->assertEquals($responseData, $this->object->execute(
            $method,
            $endPoint,
            false,
            false,
            $storeId,
            false,
            []
        ));
    }

    public function testExecute()
    {
        $method = 'post';
        $endPoint = 'https://www.feedbackcompany.com/api/v2/widgets';
        $storeId = 1;
        $params = ['type' => 'bar'];
        // @codingStandardsIgnoreLine
        $response = '{"success": true,"widget": {"shop_id": 1234,"uuid": "56c7e115-2e22-46a5-b0c2-fccb386b4ad6","type": "bar"}}';
        $responseData = [
            'success' => true,
            'widget' => [
                'shop_id' => 1234,
                'uuid' => '56c7e115-2e22-46a5-b0c2-fccb386b4ad6',
                'type' => 'bar'
            ]
        ];

        $this->feedbackConfigMock->expects($this->once())
            ->method('getAccessTokenByDefaultWay')
            ->with($storeId)
            ->willReturn(1111222233334444);
        $this->curlMock->expects($this->at(0))
            ->method('addHeader')
            ->with('Authorization', 'Bearer 1111222233334444')
            ->willReturn(true);
        $this->curlMock->expects($this->at(1))
            ->method('addHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn(true);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn('{"type":"bar"}');
        $this->curlMock->expects($this->once())
            ->method('setTimeout')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('post')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('getStatus')
            ->willReturn(200);
        $this->curlMock->expects($this->once())
            ->method('getBody')
            ->willReturn($response);
        $this->jsonMock->expects($this->once())
            ->method('unserialize')
            ->with($response)
            ->willReturn($responseData);

        $this->assertEquals($responseData, $this->object->execute(
            $method,
            $endPoint,
            true,
            true,
            $storeId,
            false,
            $params
        ));
    }

    public function testExecuteMissingAccessToken()
    {
        $method = 'post';
        $endPoint = 'https://www.feedbackcompany.com/api/v2/widgets';
        $storeId = 1;
        $params = ['type' => 'bar'];
        $response = '{"error": true,"description": "Your token has expired."}}';

        $this->expectExceptionMessage('Can\'t get new access token.');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getAccessTokenByDefaultWay')
            ->with($storeId)
            ->willReturn(1111222233334444);
        $this->curlMock->expects($this->at(0))
            ->method('addHeader')
            ->with('Authorization', 'Bearer 1111222233334444')
            ->willReturn(true);
        $this->curlMock->expects($this->at(1))
            ->method('addHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn(true);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn('{"type":"bar"}');
        $this->curlMock->expects($this->once())
            ->method('setTimeout')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('post')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('getStatus')
            ->willReturn(401);
        $this->curlMock->expects($this->once())
            ->method('getBody')
            ->willReturn($response);
        $this->accessTokenMock->expects($this->once())
            ->method('registerAccessToken')
            ->with($storeId)
            ->willReturn(false);
        $message = 'Client error: Can\'t get new access token.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->execute($method, $endPoint, true, true, $storeId, false, $params);
    }

    public function testExecuteErrorWithOutDesc()
    {
        $method = 'post';
        $endPoint = 'https://www.feedbackcompany.com/api/v2/widgets';
        $storeId = 1;
        $params = ['type' => 'bar'];
        $response = '{"error": "Your token has expired."}';
        $responseData = [
            'error' => 'Your token has expired.'
        ];

        $this->expectExceptionMessage('Your token has expired.');
        $this->feedbackConfigMock->expects($this->exactly(2))
            ->method('getAccessTokenByDefaultWay')
            ->withConsecutive(
                [$storeId],
                [$storeId]
            )
            ->willReturnOnConsecutiveCalls('1111222233334444', '5555666677778888');
        $this->curlMock->expects($this->exactly(4))
            ->method('addHeader')
            ->withConsecutive(
                ['Authorization', 'Bearer 1111222233334444'],
                ['Content-Type', 'application/json'],
                ['Authorization', 'Bearer 5555666677778888'],
                ['Content-Type', 'application/json']
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null);
        $this->jsonMock->expects($this->any())
            ->method('serialize')
            ->willReturn('{"type":"bar"}');
        $this->curlMock->expects($this->any())
            ->method('setTimeout')
            ->with(3000)
            ->willReturn(true);
        $this->curlMock->expects($this->any())
            ->method('post')
            ->willReturn(true);
        $this->curlMock->expects($this->any())
            ->method('getStatus')
            ->willReturn(401);
        $this->curlMock->expects($this->any())
            ->method('getBody')
            ->willReturn($response);
        $this->accessTokenMock->expects($this->once())
            ->method('registerAccessToken')
            ->with($storeId)
            ->willReturn(true);
        $this->feedbackConfigMock->expects($this->once())
            ->method('deleteAccessToken')
            ->with($storeId)
            ->willReturn(true);
        $this->jsonMock->expects($this->atLeastOnce())
            ->method('unserialize')
            ->with($response)
            ->willReturn($responseData);
        $message = 'Client error: Your token has expired.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->execute($method, $endPoint, true, true, $storeId, false, $params);
    }

    public function testExecuteExceptionWithDesc()
    {
        $method = 'post';
        $endPoint = 'https://www.feedbackcompany.com/api/v2/widgets';
        $storeId = 1;
        $params = ['type' => 'bar'];
        $response = '{"error": true,"description": "Your token has expired."}}';
        $responseData = [
            'error' => true,
            'description' => 'Your token has expired.'
        ];

        $this->expectExceptionMessage('Your token has expired.');
        $this->feedbackConfigMock->expects($this->exactly(2))
            ->method('getAccessTokenByDefaultWay')
            ->withConsecutive(
                [$storeId],
                [$storeId]
            )
            ->willReturnOnConsecutiveCalls('1111222233334444', '5555666677778888');
        $this->curlMock->expects($this->exactly(4))
            ->method('addHeader')
            ->withConsecutive(
                ['Authorization', 'Bearer 1111222233334444'],
                ['Content-Type', 'application/json'],
                ['Authorization', 'Bearer 5555666677778888'],
                ['Content-Type', 'application/json']
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null);
        $this->jsonMock->expects($this->any())
            ->method('serialize')
            ->willReturn('{"type":"bar"}');
        $this->curlMock->expects($this->any())
            ->method('setTimeout')
            ->with(3000)
            ->willReturn(true);
        $this->curlMock->expects($this->any())
            ->method('post')
            ->willReturn(true);
        $this->curlMock->expects($this->any())
            ->method('getStatus')
            ->willReturn(401);
        $this->curlMock->expects($this->any())
            ->method('getBody')
            ->willReturn($response);
        $this->accessTokenMock->expects($this->once())
            ->method('registerAccessToken')
            ->with($storeId)
            ->willReturn(true);
        $this->feedbackConfigMock->expects($this->once())
            ->method('deleteAccessToken')
            ->with($storeId)
            ->willReturn(true);
        $this->jsonMock->expects($this->atLeastOnce())
            ->method('unserialize')
            ->with($response)
            ->willReturn($responseData);
        $message = 'Client error: Your token has expired.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->execute($method, $endPoint, true, true, $storeId, false, $params);
    }

    public function testExecuteUnknownError()
    {
        $method = 'post';
        $endPoint = 'https://www.feedbackcompany.com/api/v2/widgets';
        $storeId = 1;
        $params = ['type' => 'bar'];
        $response = '{"status":"fail"}';
        $responseData = [
            'status' => 'fail'
        ];

        $this->expectExceptionMessage('Unknown error.');
        $this->feedbackConfigMock->expects($this->once())
            ->method('getAccessTokenByDefaultWay')
            ->with($storeId)
            ->willReturn(1111222233334444);
        $this->curlMock->expects($this->at(0))
            ->method('addHeader')
            ->with('Authorization', 'Bearer 1111222233334444')
            ->willReturn(true);
        $this->curlMock->expects($this->at(1))
            ->method('addHeader')
            ->with('Content-Type', 'application/json')
            ->willReturn(true);
        $this->jsonMock->expects($this->once())
            ->method('serialize')
            ->willReturn('{"type":"bar"}');
        $this->curlMock->expects($this->once())
            ->method('setTimeout')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('post')
            ->willReturn(true);
        $this->curlMock->expects($this->once())
            ->method('getStatus')
            ->willReturn(500);
        $this->curlMock->expects($this->once())
            ->method('getBody')
            ->willReturn($response);
        $this->jsonMock->expects($this->once())
            ->method('unserialize')
            ->with($response)
            ->willReturn($responseData);
        $message = 'Client error: Unknown error.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->object->execute($method, $endPoint, true, true, $storeId, false, $params);
    }
}
