<?php
// @codingStandardsIgnoreFile
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model;

use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Catalog\Helper\Image as CatalogImageHelper;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class TestableWidgetFilter extends WidgetFilter
{
    /**
     * @var string
     */
    public $randomPrefix;

    public function __construct(
        StoreManagerInterface $storeManager,
        Config $feedbackConfig,
        CatalogImageHelper $catalogImageHelper,
        Logger $logger,
        Widget $widget
    ) {
        parent::__construct($storeManager, $feedbackConfig, $catalogImageHelper, $logger, $widget);
    }

    /**
     * @return string
     */
    protected function getRandomPrefix()
    {
        return $this->randomPrefix;
    }
}
