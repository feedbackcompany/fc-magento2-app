<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\ResourceModel;

use FeedbackCompany\Reviews\Model\ResourceModel\Widget as ResourceWidget;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\App\ResourceConnection;
use FeedbackCompany\Reviews\Model\System\Config\Source\ExtendedWidgetType;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use FeedbackCompany\Reviews\Logger\Logger;

class WidgetTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ResourceWidget
     */
    private $object;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var AdapterInterface
     */
    private $adapterMock;

    /**
     * @var Select|\PHPUnit_Framework_MockObject_MockObject
     */
    private $selectMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $resourceMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $abstractDbMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    private $widgetTypes = [
        WidgetType::SMALL,
        WidgetType::SMALL_ONE,
        WidgetType::SMALL_TWO,
        WidgetType::BIG,
        WidgetType::BIG_ONE,
        WidgetType::BIG_TWO,
        WidgetType::BAR_TYPE,
        WidgetType::SUMMARY_TYPE,
        WidgetType::STICKY_TYPE,
        ExtendedWidgetType::POP_UP,
        ExtendedWidgetType::INLINE,
        ExtendedWidgetType::SIDEBAR
    ];

    protected function setUp() :void
    {
        $this->resourceMock = $this->getMockBuilder(ResourceConnection::class)
            ->setMethods(['getConnection', 'getMainTable', 'getTableName'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->contextMock = $this->createPartialMock(
            Context::class,
            ['getResources']
        );
        $this->contextMock->expects($this->atLeastOnce())
            ->method('getResources')
            ->willReturn($this->resourceMock);
        $this->abstractDbMock = $this->getMockForAbstractClass(
            AbstractDb::class,
            ['context' => $this->contextMock],
            '',
            true,
            true,
            true,
            [
                'getConnection',
                'getMainTable'
            ]
        );
        $this->selectMock = $this->createMock(Select::class);
        $this->adapterMock = $this->getMockForAbstractClass(
            AdapterInterface::class,
            [],
            '',
            true,
            true,
            true,
            ['select', 'isTableExists', 'beginTransaction', 'commit', 'rollBack', 'insertMultiple']
        );
        $this->loggerMock = $this->createMock(Logger::class);

        $this->resourceMock->expects($this->any())
            ->method('getTableName')
            ->with('feedback_review_widget_types')
            ->willReturn('feedback_review_widget_types');

        $this->object = new ResourceWidget(
            $this->contextMock,
            $this->loggerMock,
            \Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION
        );
    }

    public function testWidgetInstance()
    {
        $this->assertInstanceOf(AbstractDb::class, $this->object);
    }

    public function testGetWidgetByType()
    {
        $type = 'main_big_one';
        $storeId = 1;
        $bind = [
            ':widget_type' => $type,
            ':store_id' => $storeId
        ];
        $this->resourceMock->expects($this->atLeastOnce())
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->resourceMock->expects($this->atLeastOnce())
            ->method('getTableName')
            ->with('feedback_review_widget_types', 'default')
            ->willReturn('feedback_review_widget_types');
        $this->adapterMock->expects($this->once())
            ->method('select')
            ->willReturn($this->selectMock);
        $this->selectMock->expects($this->once())
            ->method('from')
            ->with('feedback_review_widget_types', 'widget_id')
            ->willReturnSelf();
        $this->selectMock->expects($this->exactly(2))
            ->method('where')
            ->withConsecutive(
                [$this->identicalTo('widget_type = :widget_type')],
                [$this->identicalTo('store_id = :store_id')]
            )
            ->willReturnSelf();
        $this->adapterMock->expects($this->once())
            ->method('fetchOne')
            ->with($this->selectMock, $bind)
            ->willReturn(5);

        $this->assertEquals(5, $this->object->getByWidgetType($type, $storeId));
    }

    public function testInsertWidgetTypesByStore()
    {
        $data = [];
        foreach ($this->widgetTypes as $type) {
            $data[] = [
                'widget_type'   => $type,
                'store_id'      => 0
            ];
        }
        $this->resourceMock->expects($this->once())
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('beginTransaction')
            ->willReturnSelf();
        $this->adapterMock->expects($this->once())
            ->method('isTableExists')
            ->with('feedback_review_widget_types')
            ->willReturn(true);
        $this->adapterMock->expects($this->once())
            ->method('insertMultiple')
            ->with('feedback_review_widget_types', $data)
            ->willReturn(1);
        $this->adapterMock->expects($this->once())
            ->method('commit')
            ->willReturnSelf();

        $this->assertInstanceOf(ResourceWidget::class, $this->object->insertWidgetTypesByStore([0]));
    }

    public function testDeleteWidgetTypes()
    {
        $this->resourceMock->expects($this->once())
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('delete')
            ->with(
                'feedback_review_widget_types',
                ['store_id = (?)' => 1]
            )
            ->willReturn(12);

        $this->assertInstanceOf(ResourceWidget::class, $this->object->deleteWidgetTypes(1));
    }

    public function testApplyException()
    {
        $data = [];
        foreach ($this->widgetTypes as $type) {
            $data[] = [
                'widget_type'   => $type,
                'store_id'      => 1
            ];
        }
        $this->resourceMock->expects($this->once())
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('beginTransaction')
            ->willReturnSelf();
        $this->adapterMock->expects($this->once())
            ->method('isTableExists')
            ->with('feedback_review_widget_types')
            ->willReturn(true);
        $this->adapterMock->expects($this->once())
            ->method('insertMultiple')
            ->with('feedback_review_widget_types', $data)
            ->willThrowException(
                new \Exception('Invalid data for insert')
            );
        $this->adapterMock->expects($this->once())
            ->method('rollBack')
            ->willReturnSelf();
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with('Invalid data for insert')
            ->willReturn(true);

        $this->assertInstanceOf(ResourceWidget::class, $this->object->insertWidgetTypesByStore([1]));
    }

    public function testDeleteWidgetTypesException()
    {
        $this->resourceMock->expects($this->once())
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('delete')
            ->with(
                'feedback_review_widget_types',
                ['store_id = (?)' => 1]
            )
            ->willThrowException(
                new \Exception('Something went wrong.')
            );
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with('Something went wrong.')
            ->willReturn(true);

        $this->assertInstanceOf(ResourceWidget::class, $this->object->deleteWidgetTypes(1));
    }

    public function testGetWidgetUuid()
    {
        $expectedResult = '90b3fe80-fc82-4836-9d1e-056b722ec558';
        $this->resourceMock->expects($this->exactly(2))
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('select')
            ->willReturn($this->selectMock);
        $this->selectMock->expects($this->once())
            ->method('from')
            ->with(
                ['t1' => 'feedback_review_widget_types'],
                new \Zend_Db_Expr('COALESCE(t1.widget_uuid, t2.widget_uuid)'),
                null
            )
            ->willReturnSelf();
        $this->selectMock->expects($this->once())
            ->method('joinLeft')
            ->with(
                ['t2' => 'feedback_review_widget_types'],
                't2.widget_type = t1.widget_type and t2.store_id = 0',
                []
            )
            ->willReturnSelf();
        $this->selectMock->expects($this->exactly(2))
            ->method('where')
            ->withConsecutive(
                [$this->identicalTo('t1.widget_type = (?)'), $this->identicalTo('main_big_two')],
                [$this->identicalTo('t1.store_id = (?)'), $this->identicalTo(1)]
            )
            ->willReturnSelf();
        $this->adapterMock->expects($this->once())
            ->method('fetchOne')
            ->with($this->selectMock)
            ->willReturn('90b3fe80-fc82-4836-9d1e-056b722ec558');
        $this->assertEquals($expectedResult, $this->object->getWidgetUuid('main_big_two', 1));
    }

    public function testGetWidgetUuidEmpty()
    {
        $this->resourceMock->expects($this->exactly(2))
            ->method('getConnection')
            ->willReturn($this->adapterMock);
        $this->adapterMock->expects($this->once())
            ->method('select')
            ->willReturn($this->selectMock);
        $this->selectMock->expects($this->once())
            ->method('from')
            ->with(
                ['t1' => 'feedback_review_widget_types'],
                new \Zend_Db_Expr('COALESCE(t1.widget_uuid, t2.widget_uuid)'),
                null
            )
            ->willReturnSelf();
        $this->selectMock->expects($this->once())
            ->method('joinLeft')
            ->with(
                ['t2' => 'feedback_review_widget_types'],
                't2.widget_type = t1.widget_type and t2.store_id = 0',
                []
            )
            ->willReturnSelf();
        $this->selectMock->expects($this->exactly(2))
            ->method('where')
            ->withConsecutive(
                [$this->identicalTo('t1.widget_type = (?)'), $this->identicalTo('small_four')],
                [$this->identicalTo('t1.store_id = (?)'), $this->identicalTo(5)]
            )
            ->willReturnSelf();
        $this->adapterMock->expects($this->once())
            ->method('fetchOne')
            ->with($this->selectMock)
            ->willReturn('');
        $this->assertEmpty($this->object->getWidgetUuid('small_four', 5));
    }

    public function testGetWidgetUuidException()
    {
        $message = 'Get Widget Uuid Error: Connection is not defined.';
        $this->resourceMock->expects($this->once())
            ->method('getConnection')
            ->willThrowException(
                new \DomainException('Connection is not defined.')
            );
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);
        $this->assertNull($this->object->getWidgetUuid('small', 0));
    }
}
