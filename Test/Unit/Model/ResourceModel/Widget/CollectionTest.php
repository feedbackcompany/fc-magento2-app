<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

declare(strict_types=1);

namespace FeedbackCompany\Reviews\Test\Unit\Model\ResourceModel\Widget;

use FeedbackCompany\Reviews\Model\ResourceModel\Widget\Collection;
use Magento\Framework\Data\Collection\EntityFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\DB\Adapter\Pdo\Mysql;
use Magento\Framework\DB\Select;

class CollectionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Collection
     */
    private $object;

    protected function setUp() :void
    {
        $entityFactoryMock = $this->createMock(EntityFactory::class);
        $loggerMock = $this->createMock(LoggerInterface::class);
        $fetchStrategyMock = $this->createMock(FetchStrategyInterface::class);
        $eventManagerMock = $this->createMock(ManagerInterface::class);
        $connectionMock = $this->createMock(Mysql::class);
        $resourceMock = $this->getMockForAbstractClass(
            AbstractDb::class,
            [],
            '',
            false,
            true,
            true,
            ['getConnection', 'getMainTable', 'getTable', '__wakeup']
        );

        $selectMock = $this->createMock(Select::class);
        $connectionMock->expects($this->any())->method('select')->will($this->returnValue($selectMock));
        $resourceMock->expects($this->any())->method('getConnection')->will($this->returnValue($connectionMock));
        $resourceMock->expects($this->any())->method('getTable')->will($this->returnArgument(0));

        $this->object = new Collection(
            $entityFactoryMock,
            $loggerMock,
            $fetchStrategyMock,
            $eventManagerMock,
            $connectionMock,
            $resourceMock
        );
    }

    public function testCollectionInstance()
    {
        $this->assertInstanceOf(AbstractCollection::class, $this->object);
    }
}
