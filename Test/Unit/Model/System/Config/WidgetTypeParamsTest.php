<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config;

use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\System\Config\WidgetTypeParams;

class WidgetTypeParamsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var WidgetTypeParams
     */
    private $object;

    protected function setUp() :void
    {
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new WidgetTypeParams($this->feedbackConfigMock);
    }

    public function testGetWidgetTypesParamsWithType()
    {
        $type = 'sidebar';
        $expectedResult[$type] = [
            'type' => 'product-extended',
            'options' => ['display_type' => 'sidebar']
        ];
        $this->assertEquals($expectedResult, $this->object->getWidgetTypesParams($type));
    }

    public function testGetWidgetTypesParams()
    {
        $expectedResult = [
            'bar' => ['type' => 'bar'],
            'product-summary' => ['type' => 'product-summary'],
            'main_small' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 0
                ]
            ],
            'main_small_one' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 1
                ]
            ],
            'main_small_two' => [
                'type' => 'main',
                'options' => [
                    'size' => 'small',
                    'amount_of_reviews' => 2
                ]
            ],
            'main_big' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 0
                ]
            ],
            'main_big_one' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 1
                ]
            ],
            'main_big_two' => [
                'type' => 'main',
                'options' => [
                    'size' => 'big',
                    'amount_of_reviews' => 2
                ]
            ],
            'sticky' => ['type' => 'sticky'],
            'popup' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'popup'
                ]
            ],
            'inline' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'inline'
                ]
            ],
            'sidebar' => [
                'type' => 'product-extended',
                'options' => [
                    'display_type' => 'sidebar'
                ]
            ]
        ];
        $this->assertEquals($expectedResult, $this->object->getWidgetTypesParams());
    }
}
