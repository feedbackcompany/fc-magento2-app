<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config;

use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Config\Model\ResourceModel\Config as SystemConfig;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use FeedbackCompany\Reviews\Logger\Logger;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\DB\Select;

class ConfigTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Config
     */
    private $object;

    /**
     * @var ReinitableConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $scopeConfigMock;

    /**
     * @var SystemConfig|\PHPUnit_Framework_MockObject_MockObject
     */
    private $systemConfigMock;

    /**
     * @var ResourceConnection|\PHPUnit_Framework_MockObject_MockObject
     */
    private $adapterMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var TypeListInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $cacheTypeListMock;

    /**
     * @var AdapterInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $adapterInterfaceMock;

    /**
     * @var Select|\PHPUnit_Framework_MockObject_MockObject
     */
    private $selectMock;

    protected function setUp() :void
    {
        $this->scopeConfigMock = $this->createMock(ReinitableConfigInterface::class);
        $this->systemConfigMock = $this->getMockBuilder(SystemConfig::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->adapterMock = $this->getMockBuilder(ResourceConnection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->loggerMock = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->cacheTypeListMock = $this->createMock(TypeListInterface::class);
        $this->adapterInterfaceMock = $this->createMock(AdapterInterface::class);
        $this->selectMock = $this->createMock(Select::class);

        $this->object = new Config(
            $this->scopeConfigMock,
            $this->systemConfigMock,
            $this->adapterMock,
            $this->loggerMock,
            $this->cacheTypeListMock
        );
    }

    public function testGetAccessTokenByDefaultWay()
    {
        $expectedResult = '454570FA3ECA20D829E5FEF9BC4B7EE322EA4E20D401ABB4ADA4C921FBC92F0D';
        $this->scopeConfigMock
            ->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getAccessTokenByDefaultWay());
    }

    public function testIsAccessTokenExist()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('isSetFlag')
            ->willReturn(true);
        $this->assertEquals(true, $this->object->isAccessTokenExist());
    }

    public function testIsShowOnPLP()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('isSetFlag')
            ->willReturn(true);
        $this->assertEquals(true, $this->object->isShowOnPLP());
    }

    public function testIsShowOnPDP()
    {
        $this->scopeConfigMock->expects($this->once())
            ->method('isSetFlag')
            ->willReturn(false);
        $this->assertSame(false, $this->object->isShowOnPDP());
    }

    public function testGetClientId()
    {
        $expectedResult = '12345123451234512345123451234589';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getClientId());
    }

    public function testGetClientSecret()
    {
        $expectedResult = '98765987659876598765987659876512';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getClientSecret());
    }

    public function testGetExtendedWidgetType()
    {
        $expectedResult = 'popup';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getExtendedWidgetType());
    }

    public function testGetDefaultEmbedCode()
    {
        $expectedResult = '<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" 
                                    id="__fbcw__{prefix}{uuid}">
                                <!-- Feedback Company Widget (end) -->';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getDefaultEmbedCode());
    }

    public function testGetProductEmbedCode()
    {
        $expectedResult = '<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" 
                                    id="__fbcw__{prefix}{uuid}">
                                    "use strict";!function(){
                                    window.FeedbackCompanyWidgets=window.FeedbackCompanyWidgets||{queue:[],loaders:[
                                    ]};var options={uuid:"{uuid}",version:"1.2.1",prefix:"{prefix}",
                                    urlParams:{product_external_id:"{product_external_id}"},
                                    templateParams:{product_name:"{product_name}",product_url:"{product_url}",
                                    product_image_url:"{product_image_url}"}};}
                                    </script>
                                <!-- Feedback Company Widget (end) -->';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getProductEmbedCode());
    }

    public function testGetInvitationTrigger()
    {
        $expectedResult = 'complete';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getInvitationTrigger());
    }

    public function testGetInvitationDelay()
    {
        $expectedResult = '5';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getInvitationDelay());
    }

    public function testGetInvitationDelayType()
    {
        $expectedResult = 'days';
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getInvitationDelayType());
    }

    public function testGetReminderDelay()
    {
        $expectedResult = 0;
        $this->scopeConfigMock->expects($this->once())
            ->method('getValue')
            ->willReturn($expectedResult);
        $this->assertEquals($expectedResult, $this->object->getReminderDelay());
    }

    public function testSaveAccessToken()
    {
        $accessToken = '111111111111111111111111111111111';
        $this->systemConfigMock->expects($this->once())
            ->method('saveConfig')
            ->with(
                'feedback_reviews/feedback_reviews_auth/access_token',
                $accessToken
            )
            ->willReturnSelf();
        $this->assertNull($this->object->saveAccessToken($accessToken, 1));
    }

    public function testDeleteAccessToken()
    {
        $this->systemConfigMock->expects($this->once())
            ->method('deleteConfig')
            ->with('feedback_reviews/feedback_reviews_auth/access_token', 'default', 0)
            ->willReturnSelf();
        $this->assertNull($this->object->deleteAccessToken(0));
    }

    public function testSaveEmbedCode()
    {
        // @codingStandardsIgnoreStart
        $embedCode = '<!-- Feedback Company Widget (start) -->
                            <script type="text/javascript" id="__fbcw__{prefix}{uuid}">
                                "use strict";!function(){
                            window.FeedbackCompanyWidgets=window.FeedbackCompanyWidgets||{queue:[],loaders:[
                            ]};var options={uuid:"{uuid}",version:"1.2.1",prefix:"{prefix}",
                            urlParams:{product_external_id:"{product_external_id}"},
                            templateParams:{product_name:"{product_name}",product_url:"{product_url}",
                            product_image_url:"{product_image_url}"}};if(
                            void 0===window.FeedbackCompanyWidget){if(
                            window.FeedbackCompanyWidgets.queue.push(options),!document.getElementById(
                            "__fbcw_FeedbackCompanyWidget")){var scriptTag=document.createElement("script")
                            ;scriptTag.onload=function(){if(window.FeedbackCompanyWidget)for(
                            ;0<window.FeedbackCompanyWidgets.queue.length;
                            )options=window.FeedbackCompanyWidgets.queue.pop(),
                            window.FeedbackCompanyWidgets.loaders.push(
                            new window.FeedbackCompanyWidgetLoader(options))},
                            scriptTag.id="__fbcw_FeedbackCompanyWidget",
                            scriptTag.src="https://www.feedbackcompany.com/includes/widgets/feedback-company-widget.min.js"
                            ,document.body.appendChild(scriptTag)}
                            }else window.FeedbackCompanyWidgets.loaders.push(
                            new window.FeedbackCompanyWidgetLoader(options))}();
                            </script>
                            <!-- Feedback Company Widget (end) -->
                            ';
        // @codingStandardsIgnoreEnd
        $this->systemConfigMock->expects($this->once())
            ->method('saveConfig')
            ->willReturnSelf();

        $this->assertNull($this->object->saveEmbedCode($embedCode, 'product'));
    }

    public function testGetAccessToken()
    {
        $expectedResult = '95175345685215998745632112345691';
        $scopeData = [
            'store' => 1,
            'scope' => 'stores'
        ];
        $this->adapterMock->expects($this->once())
            ->method('getConnection')
            ->willReturn($this->adapterInterfaceMock);
        $this->adapterInterfaceMock->expects($this->once())
            ->method('select')
            ->willReturn($this->selectMock);
        $this->adapterMock->expects($this->once())
            ->method('getTableName')
            ->with('core_config_data')
            ->willReturn('core_config_data');
        $this->selectMock->expects($this->once())
            ->method('from')
            ->with(['main' => 'core_config_data'], 'value')
            ->willReturnSelf();
        $this->selectMock->expects($this->exactly(3))
            ->method('where')
            ->withConsecutive(
            // @codingStandardsIgnoreLine
                [$this->identicalTo('path = (?)'), $this->identicalTo('feedback_reviews/feedback_reviews_auth/access_token')],
                [$this->identicalTo('scope = (?)'), $this->identicalTo($scopeData['scope'])],
                [$this->identicalTo('scope_id = (?)'), $this->identicalTo($scopeData['store'])]
            )
            ->willReturnSelf();
        $this->adapterInterfaceMock->expects($this->once())
            ->method('fetchOne')
            ->with($this->selectMock)
            ->willReturn('95175345685215998745632112345691');
        $this->assertEquals($expectedResult, $this->object->getAccessToken(1));
    }
}
