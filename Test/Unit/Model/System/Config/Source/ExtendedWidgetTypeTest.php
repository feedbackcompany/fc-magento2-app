<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use FeedbackCompany\Reviews\Model\System\Config\Source\ExtendedWidgetType;

class ExtendedWidgetTypeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ExtendedWidgetType
     */
    private $object;

    protected function setUp() :void
    {
        $objectManager = new ObjectManager($this);
        $this->object = $objectManager->getObject(ExtendedWidgetType::class);
    }

    public function testExtendedWidgetTypeInterface()
    {
        $this->assertInstanceOf(OptionSourceInterface::class, $this->object);
    }

    public function testToOptionArray()
    {
        $expectedResult = [
            ['label' => __('Inline'), 'value' => 'inline'],
            ['label' => __('Sidebar'), 'value' => 'sidebar'],
            ['label' => __('Pop-up'), 'value' => 'popup']
        ];

        $this->assertEquals($expectedResult, $this->object->toOptionArray());
    }
}
