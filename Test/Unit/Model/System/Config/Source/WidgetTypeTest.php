<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;

class WidgetTypeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetType
     */
    private $object;

    protected function setUp() :void
    {
        $objectManager = new ObjectManager($this);
        $this->object = $objectManager->getObject(WidgetType::class);
    }

    public function testWidgetTypeInterface()
    {
        $this->assertInstanceOf(OptionSourceInterface::class, $this->object);
    }

    public function testToOptionArray()
    {
        $expectedResult = [
            ['label' => __('Score (small 200px)'), 'value' => 'main_small'],
            ['label' => __('Score (small 200px) + 1 review'), 'value' => 'main_small_one'],
            ['label' => __('Score (small 200px) + 2 reviews'), 'value' => 'main_small_two'],
            ['label' => __('Score (big 300px)'), 'value' => 'main_big'],
            ['label' => __('Score (big 300px) + 1 review'), 'value' => 'main_big_one'],
            ['label' => __('Score (big 300px) + 2 reviews'), 'value' => 'main_big_two']
        ];

        $this->assertEquals($expectedResult, $this->object->toOptionArray());
    }
}
