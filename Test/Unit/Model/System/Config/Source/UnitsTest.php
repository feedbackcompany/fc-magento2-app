<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config\Source;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use FeedbackCompany\Reviews\Model\System\Config\Source\Units;
use Magento\Framework\Data\OptionSourceInterface;

class UnitsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Units
     */
    private $object;

    protected function setUp() :void
    {
        $objectManager = new ObjectManager($this);
        $this->object = $objectManager->getObject(Units::class);
    }

    public function testUnitsInterface()
    {
        $this->assertInstanceOf(OptionSourceInterface::class, $this->object);
    }

    public function testToOptionArray()
    {
        $expectedResult = [
            ['label' => __('Minutes'), 'value' => 'minutes'],
            ['label' => __('Hours'), 'value' => 'hours'],
            ['label' => __('Days'), 'value' => 'days'],
            ['label' => __('Weekdays'), 'value' => 'weekdays']
        ];

        $this->assertEquals($expectedResult, $this->object->toOptionArray());
    }
}
