<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Model\System\Config\Source;

use Magento\Sales\Model\Config\Source\Order\Status;
use FeedbackCompany\Reviews\Model\System\Config\Source\OrderStatuses;
use Magento\Sales\Model\Order\Config;

class OrderStatusesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var OrderStatuses
     */
    private $object;

    /**
     * @var Config
     */
    private $orderConfigMock;

    protected function setUp() :void
    {
        $this->orderConfigMock = $this->createMock(Config::class);
        $this->object = new OrderStatuses($this->orderConfigMock);
    }

    public function tesOrderStatusesClass()
    {
        $this->assertInstanceOf(Status::class, $this->object);
    }

    public function testToOptionArray()
    {
        $expectedResult = [
            ['value' => 0, 'label' => 'status1'],
            ['value' => 1, 'label' => 'status2']
        ];

        $this->orderConfigMock->expects($this->once())
            ->method('getStateStatuses')
            ->will($this->returnValue(['status1', 'status2']));

        $this->assertEquals($expectedResult, $this->object->toOptionArray());
    }
}
