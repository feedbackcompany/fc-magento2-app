<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Observer\Product;

use FeedbackCompany\Reviews\Observer\Product\BlockSwitcher;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\Event;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Layout;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\View\Element\AbstractBlock;

class BlockSwitcherTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var BlockSwitcher
     */
    private $object;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var Observer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $observerMock;

    /**
     * @var Event|\PHPUnit_Framework_MockObject_MockObject
     */
    private $eventMock;

    /**
     * @var Layout|\PHPUnit_Framework_MockObject_MockObject
     */
    private $layout;

    /**
     * @var StoreInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeMock;

    /**
     * @var AbstractBlock|\PHPUnit_Framework_MockObject_MockObject
     */
    private $blockMock;

    protected function setUp() :void
    {
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->observerMock = $this
            ->getMockBuilder(\Magento\Framework\Event\Observer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock = $this
            ->getMockBuilder(\Magento\Framework\Event::class)
            ->setMethods(['getLayout', 'getFullActionName'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->layout = $this->getMockBuilder(Layout::class)
            ->setMethods(['getBlock', 'unsetElement'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->blockMock = $this->getMockBuilder(AbstractBlock::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new BlockSwitcher($this->feedbackConfigMock, $this->storeManagerMock);
    }

    public function testBlockSwitcherInterface()
    {
        $this->assertInstanceOf(ObserverInterface::class, $this->object);
    }

    public function testExecute()
    {
        $this->observerMock->expects($this->exactly(2))
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getLayout')
            ->willReturn($this->layout);
        $this->eventMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('cms_index_index');
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));
        $result = $this->object->execute($this->observerMock);
        $this->assertNull($result);
    }

    public function testExecuteWidgetTypeInline()
    {
        $storeId = 1;
        $blockName = 'feedback.reviews.extended';
        $this->observerMock->expects($this->exactly(2))
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getLayout')
            ->willReturn($this->layout);
        $this->eventMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('catalog_product_view');
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->will($this->returnValue($storeId));
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->with(1)
            ->willReturn(true);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getExtendedWidgetType')
            ->with($storeId)
            ->willReturn('inline');
        $this->layout->expects($this->atLeastOnce())
            ->method('getBlock')
            ->with($blockName)
            ->willReturn($this->blockMock);
        $this->layout->expects($this->once())
            ->method('unsetElement')
            ->with($blockName)
            ->willReturnSelf();

        $result = $this->object->execute($this->observerMock);
        $this->assertNull($result);
    }

    public function testExecuteWidgetTypeSidebar()
    {
        $storeId = 1;
        $blockName = 'feedback.reviews.tab';
        $this->observerMock->expects($this->exactly(2))
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getLayout')
            ->willReturn($this->layout);
        $this->eventMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('catalog_product_view');
        $this->storeManagerMock->expects($this->once())
            ->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->will($this->returnValue($storeId));
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->with(1)
            ->willReturn(true);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getExtendedWidgetType')
            ->with($storeId)
            ->willReturn('sidebar');
        $this->layout->expects($this->atLeastOnce())
            ->method('getBlock')
            ->with($blockName)
            ->willReturn($this->blockMock);
        $this->layout->expects($this->once())
            ->method('unsetElement')
            ->with($blockName)
            ->willReturnSelf();

        $result = $this->object->execute($this->observerMock);
        $this->assertNull($result);
    }
}
