<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Observer\Adminhtml;

use FeedbackCompany\Reviews\Observer\Adminhtml\ConfigState;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\AccessToken;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event;

class ConfigStateTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var AccessToken|\PHPUnit_Framework_MockObject_MockObject
     */
    private $accessTokenMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var ConfigState
     */
    private $object;

    /**
     * @var Observer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $observerMock;

    /**
     * @var Event|\PHPUnit\Framework\MockObject\MockObject
     */
    private $eventMock;

    /**
     * @var \ReflectionClass
     */
    private $class;

    protected function setUp() :void
    {
        $this->accessTokenMock = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->observerMock = $this
            ->getMockBuilder(\Magento\Framework\Event\Observer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock = $this
            ->getMockBuilder(\Magento\Framework\Event::class)
            ->setMethods(['getChangedPaths', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new ConfigState($this->accessTokenMock, $this->feedbackConfigMock);
    }

    public function testConfigStateInterface()
    {
        $this->assertInstanceOf(ObserverInterface::class, $this->object);
    }

    public function testExecute()
    {
        $changedPaths = [
            'feedback_reviews/feedback_reviews_auth/client_id',
            'feedback_reviews/feedback_reviews_auth/client_secret'
        ];
        $storeId = 0;

        $this->observerMock->expects($this->any())
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getChangedPaths')
            ->willReturn($changedPaths);
        $this->eventMock->expects($this->any())
            ->method('getStore')
            ->willReturn($storeId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getAccessToken')
            ->with($storeId)
            ->willReturn(false);
        $this->accessTokenMock->expects($this->once())
            ->method('registerAccessToken')
            ->with($storeId)
            ->willReturn(true);

        $class = new \ReflectionClass(ConfigState::class);
        $property = $class->getProperty('shouldUpdateToken');
        $property->setAccessible(true);
        $property->setValue($this->object, false);
        $result = $this->object->execute($this->observerMock);
        $this->assertNull($result);
    }

    public function testExecuteUpdateSystemConfig()
    {
        $changedPaths = [
            'feedback_reviews/feedback_reviews_auth/client_secret'
        ];
        $storeId = 0;

        $this->observerMock->expects($this->any())
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock->expects($this->once())
            ->method('getChangedPaths')
            ->willReturn($changedPaths);
        $this->eventMock->expects($this->any())
            ->method('getStore')
            ->willReturn($storeId);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getAccessToken')
            ->with($storeId)
            ->willReturn(true);
        $this->accessTokenMock->expects($this->once())
            ->method('registerAccessToken')
            ->with($storeId)
            ->willReturn(true);

        $result = $this->object->execute($this->observerMock);
        $this->assertNull($result);
    }
}
