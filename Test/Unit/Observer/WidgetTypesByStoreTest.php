<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Observer;

use FeedbackCompany\Reviews\Observer\WidgetTypesByStore;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;
use Magento\Store\Model\Store;

class WidgetTypesByStoreTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetTypesByStore
     */
    private $object;

    /**
     * @var Observer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $observerMock;

    /**
     * @var Widget|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetMock;

    /**
     * @var Store|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeMock;

    protected function setUp() :void
    {
        $this->observerMock = $this->createMock(Observer::class);
        $this->widgetMock = $this->createMock(Widget::class);
        $this->storeMock = $this->createMock(Store::class);

        $this->object = new WidgetTypesByStore($this->widgetMock);
    }

    public function testExecute()
    {
        $this->observerMock->expects($this->once())
            ->method('getData')
            ->with('store')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->widgetMock->expects($this->once())
            ->method('insertWidgetTypesByStore')
            ->with([1])
            ->willReturnSelf();

        $this->assertInstanceOf(WidgetTypesByStore::class, $this->object->execute($this->observerMock));
    }

    public function testWidgetTypesInstance()
    {
        $this->assertInstanceOf(ObserverInterface::class, $this->object);
    }
}
