<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Plugin\Sales\Model\ResourceModel;

use Magento\Sales\Model\ResourceModel\Order as SalesOrderResource;
use Magento\Sales\Model\Order as SalesOrder;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\RegisterOrder;
use FeedbackCompany\Reviews\Plugin\Sales\Model\ResourceModel\Order as PluginOrder;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SalesOrder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $orderModelMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var RegisterOrder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $registerOrderMock;

    /**
     * @var PluginOrder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $object;

    /**
     * @var SalesOrderResource|\PHPUnit_Framework_MockObject_MockObject
     */
    private $orderResourceMock;

    protected function setUp() :void
    {
        $this->registerOrderMock = $this->createMock(RegisterOrder::class);
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderResourceMock = $this->createMock(SalesOrderResource::class);
        $this->orderModelMock = $this->createMock(SalesOrder::class);

        $this->object = new PluginOrder($this->feedbackConfigMock, $this->registerOrderMock);
    }

    public function testBeforeSave()
    {
        $class = new \ReflectionClass(PluginOrder::class);
        $property = $class->getProperty('orderModel');
        $property->setAccessible(true);
        $this->object->beforeSave($this->orderResourceMock, $this->orderModelMock);
        $this->assertEquals($this->orderModelMock, $property->getValue($this->object));
    }

    public function testAfterSave()
    {
        $this->object->beforeSave($this->orderResourceMock, $this->orderModelMock);
        $this->orderModelMock->expects($this->once())
            ->method('getStoreId')
            ->willReturn(1);
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $this->feedbackConfigMock->expects($this->once())
            ->method('getInvitationTrigger')
            ->willReturn('complete');
        $this->orderModelMock->expects($this->once())
            ->method('getStatus')
            ->willReturn('complete');
        $this->registerOrderMock->expects($this->once())
            ->method('registerOrder')
            ->with($this->orderModelMock)
            ->willReturnSelf();

        $abstractDbMock = $this->getMockBuilder(AbstractDb::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->assertSame($abstractDbMock, $this->object->afterSave($this->orderResourceMock, $abstractDbMock));
    }

    public function testAfterSaveMissingOrderModel()
    {
        $abstractDbMock = $this->getMockBuilder(AbstractDb::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->assertSame($abstractDbMock, $this->object->afterSave($this->orderResourceMock, $abstractDbMock));
    }
}
