<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Plugin;

use FeedbackCompany\Reviews\Plugin\ProductSummary;
use Magento\Review\Block\Product\ReviewRenderer;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Framework\App\RequestInterface;
use FeedbackCompany\Reviews\Block\Widget\Type\ProductWidget;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Catalog\Model\Product;

class ProductSummaryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ReviewRenderer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $reviewRendererMock;

    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var ProductWidget|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productWidgetMock;

    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeMock;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $productMock;

    /**
     * @var ProductSummary
     */
    private $object;

    protected function setUp() :void
    {
        $this->storeManagerMock = $this->createMock(\Magento\Store\Model\StoreManagerInterface::class);
        $this->feedbackConfigMock = $this->getMockBuilder(Config::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->setMethods(['create', 'getFullActionName'])
            ->getMockForAbstractClass();
        $this->productWidgetMock = $this->getMockBuilder(ProductWidget::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();

        $this->reviewRendererMock = $this->getMockBuilder(ReviewRenderer::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productMock = $this->createMock(Product::class);

        $this->object = new ProductSummary(
            $this->storeManagerMock,
            $this->feedbackConfigMock,
            $this->requestMock,
            $this->productWidgetMock
        );
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testAfterGetReviewsSummaryHtmlSuccess()
    {
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->storeManagerMock->expects($this->once())->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())->method('getId')
            ->will($this->returnValue(1));
        $this->reviewRendererMock = $this->getMockBuilder(ReviewRenderer::class)
            ->setMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        $this->productFactory = $this->getMockBuilder(ProductFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $this->requestMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('cms_index_index');
        $this->productWidgetMock->expects($this->once())
            ->method('getReviewsSummaryHtml')
            ->with($this->productMock)
            ->willReturn('<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" id="__fbcw__{prefix}{uuid}">
                                <!-- Feedback Company Widget (end) -->');

        $result = '<!-- Feedback Company Widget (start) -->
                                    <script type="text/javascript" id="__fbcw__{prefix}{uuid}">
                                <!-- Feedback Company Widget (end) -->';

        $this->assertSame($result, $this->object->afterGetReviewsSummaryHtml(
            $this->reviewRendererMock,
            $result,
            $this->productMock
        ));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testAfterGetReviewsSummaryHtmlMissingToken()
    {
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->storeManagerMock->expects($this->once())->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())->method('getId')
            ->will($this->returnValue(1));
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->willReturn(false);

        $this->assertEmpty($this->object->afterGetReviewsSummaryHtml(
            $this->reviewRendererMock,
            '',
            $this->productMock
        ));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testAfterGetReviewsSummaryHtmlShowOnPLP()
    {
        $storeId = 1;
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->storeManagerMock->expects($this->once())->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())->method('getId')
            ->will($this->returnValue($storeId));
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $this->requestMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('catalog_category_view');
        $this->feedbackConfigMock->expects($this->once())
            ->method('isShowOnPLP')
            ->with($storeId)
            ->willReturn('');

        $this->assertEmpty($this->object->afterGetReviewsSummaryHtml(
            $this->reviewRendererMock,
            '',
            $this->productMock
        ));
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testAfterGetReviewsSummaryHtmlShowOnPDP()
    {
        $storeId = 1;
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->storeManagerMock->expects($this->once())->method('getStore')
            ->will($this->returnValue($this->storeMock));
        $this->storeMock->expects($this->once())->method('getId')
            ->will($this->returnValue($storeId));
        $this->feedbackConfigMock->expects($this->once())
            ->method('isActive')
            ->willReturn(true);
        $this->requestMock->expects($this->once())
            ->method('getFullActionName')
            ->willReturn('catalog_product_view');
        $this->feedbackConfigMock->expects($this->once())
            ->method('isShowOnPDP')
            ->with($storeId)
            ->willReturn('');

        $this->assertEmpty($this->object->afterGetReviewsSummaryHtml(
            $this->reviewRendererMock,
            '',
            $this->productMock
        ));
    }
}
