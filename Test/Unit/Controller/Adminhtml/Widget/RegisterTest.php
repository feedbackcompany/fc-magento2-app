<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Controller\Adminhtml\Widget;

use FeedbackCompany\Reviews\Controller\Adminhtml\Widget\Register;
use Magento\Framework\App\RequestInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetType;
use Magento\Framework\Controller\Result\Json;

class RegisterTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Register
     */
    private $object;

    /**
     * @var RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $requestMock;

    /**
     * @var Context|\PHPUnit_Framework_MockObject_MockObject
     */
    private $contextMock;

    /**
     * @var JsonFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $jsonFactoryMock;

    /**
     * @var Json|\PHPUnit\Framework\MockObject\MockObject
     */
    private $jsonMock;

    /**
     * @var WidgetType|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetTypeMock;

    /**
     * @var Action|\PHPUnit_Framework_MockObject_MockObject
     */
    private $actionMock;

    protected function setUp() :void
    {
        $this->requestMock = $this->getMockBuilder(RequestInterface::class)
            ->setMethods(['isAjax'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->contextMock = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->widgetTypeMock = $this->createMock(WidgetType::class);
        $this->actionMock = $this->createMock(Action::class);
        $this->jsonFactoryMock = $this->getMockBuilder(JsonFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->jsonMock = $this->getMockBuilder(Json::class)
            ->setMethods(['create', 'setData'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->contextMock->expects($this->any())->method('getRequest')->willReturn($this->requestMock);
        $this->jsonFactoryMock->expects($this->any())->method('create')->willReturn($this->jsonMock);

        $this->object = new Register($this->contextMock, $this->jsonFactoryMock, $this->widgetTypeMock);
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(Action::class, $this->object);
    }

    public function testExecute()
    {
        $expectedData = [
            'status' => 'Successful'
        ];
        $this->requestMock->expects($this->once())
            ->method('isAjax')
            ->willReturn(true);
        $this->requestMock->expects($this->atLeastOnce())
            ->method('getParam')
            ->with('store')
            ->willReturn(1);
        $this->widgetTypeMock->expects($this->once())
            ->method('registerWidgets')
            ->with(1)
            ->willReturn(true);
        $this->jsonMock->expects($this->once())
            ->method('setData')
            ->with($expectedData)
            ->willReturnSelf();

        $this->assertSame($this->jsonMock, $this->object->execute());
    }

    public function testExecuteException()
    {
        $expectedData = [
            'status' => 'Failed',
            'error_message' => __('Registration is failed. Please connect with Feedback Company.')
        ];
        $this->requestMock->expects($this->once())
            ->method('isAjax')
            ->willReturn(true);
        $this->requestMock->expects($this->atLeastOnce())
            ->method('getParam')
            ->with('store')
            ->willReturn(1);
        $this->widgetTypeMock->expects($this->once())
            ->method('registerWidgets')
            ->with(1)
            ->willThrowException(
                new \Exception('Some Exception.')
            );
        $this->jsonMock->expects($this->once())
            ->method('setData')
            ->with($expectedData)
            ->willReturnSelf();
        $this->assertSame($this->jsonMock, $this->object->execute());
    }
}
