<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Cron;

use FeedbackCompany\Reviews\Cron\CheckWidgetRegistration;
use FeedbackCompany\Reviews\Model\WidgetRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetType;
use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Api\Data\WidgetSearchResultInterface;
use FeedbackCompany\Reviews\Model\Widget;
use FeedbackCompany\Reviews\Api\Data\WidgetInterface;

class CheckWidgetRegistrationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var WidgetRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetRepositoryMock;

    /**
     * @var SearchCriteriaBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $searchCriteriaMock;

    /**
     * @var FilterBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $filterBuilderMock;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $feedbackConfigMock;

    /**
     * @var WidgetType|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetTypeMock;

    /**
     * @var Logger|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var WidgetSearchResultInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetSearchResultMock;

    /**
     * @var Widget|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetMock;

    /**
     * @var WidgetInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetInterfaceMock;
    /**
     * @var CheckWidgetRegistration|\PHPUnit_Framework_MockObject_MockObject
     */
    private $object;

    protected function setUp() :void
    {
        $this->widgetRepositoryMock = $this->createMock(WidgetRepository::class);
        $this->searchCriteriaMock = $this->getMockBuilder(SearchCriteriaBuilder::class)
            ->setMethods(['addFilters', 'create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->filterBuilderMock = $this->getMockBuilder(FilterBuilder::class)
            ->setMethods(['setField', 'setConditionType', 'create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->feedbackConfigMock = $this->createMock(Config::class);
        $this->widgetTypeMock = $this->createMock(WidgetType::class);
        $this->loggerMock = $this->createMock(Logger::class);
        $this->widgetSearchResultMock = $this->createMock(WidgetSearchResultInterface::class);
        $this->widgetMock = $this->createMock(Widget::class);
        $this->widgetInterfaceMock = $this->createMock(WidgetInterface::class);

        $this->object = new CheckWidgetRegistration(
            $this->widgetRepositoryMock,
            $this->searchCriteriaMock,
            $this->filterBuilderMock,
            $this->feedbackConfigMock,
            $this->widgetTypeMock,
            $this->loggerMock
        );
    }

    public function testExecute()
    {
        $list = [
            'items' => $this->widgetInterfaceMock
        ];
        $this->filterBuilderMock->expects($this->once())
            ->method('setField')
            ->with('widget_uuid')
            ->willReturnSelf();
        $this->filterBuilderMock->expects($this->once())
            ->method('setConditionType')
            ->with('null')
            ->willReturnSelf();
        $this->filterBuilderMock->expects($this->once())
            ->method('create')
            ->willReturnSelf();
        $this->searchCriteriaMock->expects($this->once())
            ->method('addFilters')
            ->with([$this->filterBuilderMock])
            ->willReturnSelf();
        $searchCriteriaMock = $this->createMock(\Magento\Framework\Api\SearchCriteria::class);
        $this->searchCriteriaMock->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);
        $this->widgetRepositoryMock->expects($this->once())
            ->method('getList')
            ->with($searchCriteriaMock)
            ->willReturn($this->widgetSearchResultMock);
        $this->widgetSearchResultMock->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(1);
        $this->widgetSearchResultMock->expects($this->once())
            ->method('getItems')
            ->willReturn($list);
        $this->widgetInterfaceMock->expects($this->exactly(3))
            ->method('getStoreId')
            ->willReturn(1);
        $this->widgetInterfaceMock->expects($this->exactly(1))
            ->method('getWidgetType')
            ->willReturn('main_small');
        $this->feedbackConfigMock->expects($this->exactly(1))
            ->method('isActive')
            ->with(1)
            ->willReturn(true);
        $this->widgetTypeMock->expects($this->exactly(1))
            ->method('registerWidgets')
            ->willReturn(true);

        $this->assertNull($this->object->execute());
    }

    public function testExecuteException()
    {
        $list = [
            'items' => $this->widgetInterfaceMock
        ];
        $this->filterBuilderMock->expects($this->once())
            ->method('setField')
            ->with('widget_uuid')
            ->willReturnSelf();
        $this->filterBuilderMock->expects($this->once())
            ->method('setConditionType')
            ->with('null')
            ->willReturnSelf();
        $this->filterBuilderMock->expects($this->once())
            ->method('create')
            ->willReturnSelf();
        $this->searchCriteriaMock->expects($this->once())
            ->method('addFilters')
            ->with([$this->filterBuilderMock])
            ->willReturnSelf();
        $searchCriteriaMock = $this->createMock(\Magento\Framework\Api\SearchCriteria::class);
        $this->searchCriteriaMock->expects($this->once())
            ->method('create')
            ->willReturn($searchCriteriaMock);
        $this->widgetRepositoryMock->expects($this->once())
            ->method('getList')
            ->with($searchCriteriaMock)
            ->willReturn($this->widgetSearchResultMock);
        $this->widgetSearchResultMock->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(1);
        $this->widgetSearchResultMock->expects($this->once())
            ->method('getItems')
            ->willReturn($list);
        $this->widgetInterfaceMock->expects($this->exactly(3))
            ->method('getStoreId')
            ->willReturn(1);
        $this->widgetInterfaceMock->expects($this->exactly(1))
            ->method('getWidgetType')
            ->willReturn('main_small');
        $this->feedbackConfigMock->expects($this->exactly(1))
            ->method('isActive')
            ->with(1)
            ->willReturn(true);
        $this->widgetTypeMock->expects($this->exactly(1))
            ->method('registerWidgets')
            ->willThrowException(
                new \Exception('Missing widget type in response.')
            );
        $message = 'Cron error: Missing widget type in response.';
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->assertNull($this->object->execute());
    }
}
