<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Cron;

use FeedbackCompany\Reviews\Cron\UpdateEmbedCodes;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetEmbedCodes;
use FeedbackCompany\Reviews\Logger\Logger;

class UpdateEmbedCodesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $object;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetCodesMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    protected function setUp() :void
    {
        $this->widgetCodesMock = $this->createMock(WidgetEmbedCodes::class);
        $this->loggerMock = $this->createMock(Logger::class);

        $this->object = new UpdateEmbedCodes($this->widgetCodesMock, $this->loggerMock);
    }

    public function testExecute()
    {
        $this->widgetCodesMock->expects($this->once())
            ->method('registerEmbedCodes')
            ->willReturn(true);

        $this->assertNull($this->object->execute());
    }

    public function testExecuteException()
    {
        $message = 'Cron error: Can\'t get new access token.';
        $this->widgetCodesMock->expects($this->once())
            ->method('registerEmbedCodes')
            ->willThrowException(
                new \Exception('Can\'t get new access token.')
            );
        $this->loggerMock->expects($this->once())
            ->method('error')
            ->with($message)
            ->willReturn(true);

        $this->assertNull($this->object->execute());
    }
}
