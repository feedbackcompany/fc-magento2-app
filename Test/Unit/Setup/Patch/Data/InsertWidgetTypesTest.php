<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Test\Unit\Setup\Patch\Data;

use FeedbackCompany\Reviews\Setup\Patch\Data\InsertWidgetTypes;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use FeedbackCompany\Reviews\Model\System\Config\Source\ExtendedWidgetType;
use Magento\Store\Api\Data\StoreInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class InsertWidgetTypesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var InsertWidgetTypes
     */
    private $object;

    /**
     * @var ModuleDataSetupInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $moduleDataSetupMock;

    /**
     * @var StoreManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeManagerMock;

    /**
     * @var StoreInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $storeMock;

    /**
     * @var Widget|\PHPUnit_Framework_MockObject_MockObject
     */
    private $widgetMock;

    protected function setUp() :void
    {
        $this->moduleDataSetupMock = $this->createMock(ModuleDataSetupInterface::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->widgetMock = $this->createMock(Widget::class);
        $this->object = new InsertWidgetTypes(
            $this->moduleDataSetupMock,
            $this->storeManagerMock,
            $this->widgetMock
        );
    }

    public function testApply()
    {
        $storeList = [
            'storeList' => $this->storeMock
        ];
        $this->moduleDataSetupMock->expects($this->once())
            ->method('startSetup')
            ->willReturnSelf();
        $this->storeManagerMock->expects($this->once())
            ->method('getStores')
            ->with(true)
            ->willReturn($storeList);
        $this->storeMock->expects($this->any())
            ->method('getId')
            ->willReturn(0);
        $this->widgetMock->expects($this->once())
            ->method('insertWidgetTypesByStore')
            ->with([0])
            ->willReturnSelf();
        $this->moduleDataSetupMock->expects($this->once())
            ->method('endSetup')
            ->willReturnSelf();

        $this->assertEmpty($this->object->apply());
    }

    public function testGetAliases()
    {
        $this->assertEmpty($this->object->getAliases());
    }

    public function testGetDependencies()
    {
        $this->assertEmpty($this->object::getDependencies());
    }

    public function testInsertWidgetsInstanceOf()
    {
        $this->assertInstanceOf(DataPatchInterface::class, $this->object);
    }
}
