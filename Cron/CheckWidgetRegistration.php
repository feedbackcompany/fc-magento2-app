<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Cron;

use FeedbackCompany\Reviews\Model\WidgetRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\WidgetType;
use FeedbackCompany\Reviews\Logger\Logger;
use FeedbackCompany\Reviews\Api\Data\WidgetInterface;

class CheckWidgetRegistration
{
    /**
     * @var WidgetRepository
     */
    private $widgetRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var WidgetType
     */
    private $widgetType;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        WidgetRepository $widgetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Config $config,
        WidgetType $widgetType,
        Logger $logger
    ) {
        $this->widgetRepository      = $widgetRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder         = $filterBuilder;
        $this->feedbackConfig        = $config;
        $this->widgetType            = $widgetType;
        $this->logger                = $logger;
    }

    /**
     * Cron
     * Check widget registration
     */
    public function execute()
    {
        try {
            $filter = $this->filterBuilder
                ->setField(WidgetInterface::WIDGET_UUID)
                ->setConditionType('null')
                ->create();

            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilters([$filter])
                ->create();

            $collection = $this->widgetRepository->getList($searchCriteria);

            if ($collection->getTotalCount() > 0) {
                /** @var \FeedbackCompany\Reviews\Model\Widget $widget */
                foreach ($collection->getItems() as $widget) {
                    $storeId = $widget->getStoreId() ? $widget->getStoreId() : 0;
                    if ($this->feedbackConfig->isActive($storeId)) {
                        $this->widgetType->registerWidgets($widget->getStoreId(), $widget->getWidgetType());
                    }
                }
            }
        } catch (\Exception $e) {
            $message = 'Cron error: ' . $e->getMessage();
            $this->logger->error($message);
        }
    }
}
