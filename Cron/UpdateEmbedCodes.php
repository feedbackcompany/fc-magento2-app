<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Cron;

use FeedbackCompany\Reviews\Model\Api\Data\WidgetEmbedCodes;
use FeedbackCompany\Reviews\Logger\Logger;

class UpdateEmbedCodes
{
    /**
     * @var WidgetEmbedCodes
     */
    private $widgetEmbedCodes;

    /**
     * @var Logger
     */
    private $logger;

    public function __construct(WidgetEmbedCodes $widgetEmbedCodes, Logger $logger)
    {
        $this->widgetEmbedCodes = $widgetEmbedCodes;
        $this->logger = $logger;
    }

    /**
     * Cron
     * Refresh widget embed codes daily
     */
    public function execute()
    {
        try {
            $this->widgetEmbedCodes->registerEmbedCodes();
        } catch (\Exception $e) {
            $message = 'Cron error: ' . $e->getMessage();
            $this->logger->error($message);
        }
    }
}
