<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class InsertWidgetTypes implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Widget
     */
    private $widget;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        StoreManagerInterface $storeManager,
        Widget $widget
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeManager    = $storeManager;
        $this->widget          = $widget;
    }

    /**
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $storeList = $this->storeManager->getStores(true);
        if ($storeList) {
            $storeIds = [];
            foreach ($storeList as $store) {
                $storeIds[] = $store->getId();
            }

            if (count($storeIds) > 0) {
                $this->widget->insertWidgetTypesByStore($storeIds);
            }
        }
        $this->moduleDataSetup->endSetup();
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}
