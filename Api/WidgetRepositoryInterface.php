<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WidgetRepositoryInterface
{
    /**
     * Save widget data
     *
     * @param Data\WidgetInterface $widgetType
     * @return mixed
     */
    public function save(Data\WidgetInterface $widgetType);

    /**
     * Get widget data by widget type
     *
     * @param string $widgetType
     * @param int $storeId
     * @return mixed
     */
    public function getByWidgetType($widgetType, $storeId);

    /**
     * Get collection of widgets data
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
