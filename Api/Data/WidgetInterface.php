<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Api\Data;

interface WidgetInterface
{
    const WIDGET_ID = 'widget_id';
    const WIDGET_TYPE = 'widget_type';
    const WIDGET_UUID = 'widget_uuid';
    const STORE_ID = 'store_id';

    /**
     * @param int $widgetId
     * @return $this
     */
    public function setWidgetId($widgetId);

    /**
     * @param string $widgetType
     * @return $this
     */
    public function setWidgetType($widgetType);

    /**
     * @param string $widgetUuid
     * @return $this
     */
    public function setWidgetUuid($widgetUuid);

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * @return int
     */
    public function getWidgetId();

    /**
     * @return string
     */
    public function getWidgetType();

    /**
     * @return string
     */
    public function getWidgetUuid();

    /**
     * @return int
     */
    public function getStoreId();
}
