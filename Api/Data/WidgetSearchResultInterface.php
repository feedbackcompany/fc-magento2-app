<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;
use FeedbackCompany\Reviews\Api\Data\WidgetInterface;

interface WidgetSearchResultInterface extends SearchResultsInterface
{
    /**
     * Get widget list
     *
     * @return WidgetInterface[]
     */
    public function getItems();

    /**
     * Set widget list
     *
     * @param WidgetInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
