<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Backend\Block\Template\Context;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class Status extends Field
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ScopeConfigInterface;
     */
    private $scopeConfig;

    /**
     * @var Config
     */
    private $feedbackConfig;

    public function __construct(
        Context $context,
        RequestInterface $request,
        ScopeConfigInterface $scopeConfig,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->request        = $request;
        $this->scopeConfig    = $scopeConfig;
        $this->feedbackConfig = $config;
    }

    /**
     * Render element value
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _renderValue(AbstractElement $element)
    {
        $storeId = $this->request->getParam('store') ? $this->request->getParam('store') : 0;

        if ($this->feedbackConfig->getAccessToken($storeId)) {
            $html = '<td class="value">';
            $html .= '<div class="status-container"><span class="status-active" 
                        style="color: #00ff11">' . __('Active') . '</span></div>';
        } else {
            $html = '<td class="value">';
            $html .= '<div class="status-container"><span class="status-inactive" 
                        style="color: #ff0000">' . __('Inactive') . '</span></div>';
        }

        $html .= '<p class="note">
                    <span>' . __('Status of selected store view.') .
            '</span></p>';
        $html .= '</td>';

        return $html;
    }

    /**
     * Render inheritance checkbox (Use Default or Use Website)
     *
     * Disable rendering checkbox for field - status
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _renderInheritCheckbox(AbstractElement $element)
    {
        return '';
    }
}
