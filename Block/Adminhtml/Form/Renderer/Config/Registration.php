<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\RequestInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Exception\LocalizedException;

class Registration extends Field
{
    /**
     * Template for button
     *
     * @var string
     */
    protected $_template = 'FeedbackCompany_Reviews::system/config/registration-button.phtml';

    /**
     * @var Json
     */
    protected $_jsonConverter;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var Config
     */
    protected $_feedbackConfig;

    public function __construct(
        Context $context,
        Json $json,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_jsonConverter = $json;
        $this->_request = $context->getRequest();
        $this->_feedbackConfig = $config;
    }

    /**
     * Render element
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Url for ajax call
     *
     * @return bool|false|string
     */
    public function getAjaxUrl()
    {
        $storeId = $this->_request->getParam('store') ? $this->_request->getParam('store') : 0;
        return $this->_jsonConverter
            ->serialize($this->getUrl('feedback/widget/register', ['store' => $storeId]));
    }

    /**
     * Get html of current element
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Get html for registration button
     *
     * @return string
     * @throws LocalizedException
     */
    public function getButtonHtml()
    {
        $html = '';
        $storeId = $this->_request->getParam('store') ? $this->_request->getParam('store') : 0;

        if ($this->_feedbackConfig->getAccessToken($storeId)) {
            $button = $this->getLayout()->createBlock(Button::class)
                ->setData(
                    [
                        'id'     => 'registration_button',
                        'label'  => __('Register')
                    ]
                );

            $html = $button->toHtml();
            $html .= '<p class="note">
                        <span>'
                            . __('Successfully connected! Press the button to register your widgets.')
                        . '</span>
                     </p>';
        }

        return $html;
    }
}
