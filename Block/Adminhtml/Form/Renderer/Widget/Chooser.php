<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Widget;

use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

class Chooser extends Extended
{
    /**
     * Set additional html structure to the widget form
     *
     * @param AbstractElement $element
     * @return AbstractElement
     * @throws LocalizedException
     */
    public function prepareElementHtml(AbstractElement $element)
    {
        $type = $this->getLayout()->createBlock(
            WidgetTypeImage::class
        );

        $element->setData('after_element_html', $type->toHtml());
        return $element;
    }
}
