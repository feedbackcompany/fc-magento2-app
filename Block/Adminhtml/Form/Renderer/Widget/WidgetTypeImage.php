<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Adminhtml\Form\Renderer\Widget;

use Magento\Backend\Block\Template;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType as SourceModel;
use Magento\Framework\Serialize\Serializer\Json;

class WidgetTypeImage extends Template
{
    /**
     * @var Json
     */
    private $jsonConverter;

    public function __construct(
        Template\Context $context,
        Json $json,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsonConverter    = $json;
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('widget/widget-type.phtml');
    }

    /**
     * Transfer data to ui component
     *
     * @return bool|false|string
     */
    public function getWidgetConfig()
    {
        return $this->jsonConverter->serialize($this->getPreviewConfig());
    }

    /**
     * Config for widget preview images
     *
     * @return array
     */
    private function getPreviewConfig()
    {
        return [
            SourceModel::SMALL     => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_small_0_reviews.png'),
            SourceModel::SMALL_ONE => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_small_1_review.png'),
            SourceModel::SMALL_TWO => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_small_2_reviews.png'),
            SourceModel::BIG       => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_big_0_reviews.png'),
            SourceModel::BIG_ONE   => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_big_1_review.png'),
            SourceModel::BIG_TWO   => $this->getViewFileUrl('FeedbackCompany_Reviews::images/main_big_2_reviews.png')
        ];
    }
}
