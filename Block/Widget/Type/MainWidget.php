<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Widget\Type;

use FeedbackCompany\Reviews\Block\Widget\AbstractWidget;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class MainWidget extends AbstractWidget implements BlockInterface
{
    public function __construct(
        Template\Context $context,
        WidgetFilter $widgetFilter,
        Registry $registry,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $widgetFilter, $registry, $config, $data);
    }

    /**
     * Get widget embed code
     *
     * @return string
     */
    public function getWidgetCode()
    {
        $widgetCode = '';
        $widgetType = $this->getData('widget_type');

        if ($widgetType) {
            $widgetCode = $this->_widgetFilter->prepareDefaultCode($widgetType);
        }

        return $widgetCode ? $widgetCode : '';
    }
}
