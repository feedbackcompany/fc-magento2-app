<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Block\Widget\Type;

use FeedbackCompany\Reviews\Block\Widget\AbstractWidget;
use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use FeedbackCompany\Reviews\Model\System\Config\Config;

class ProductWidget extends AbstractWidget
{
    public function __construct(
        Template\Context $context,
        WidgetFilter $widgetFilter,
        Registry $registry,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $widgetFilter, $registry, $config, $data);
    }

    /**
     * Get html structure
     *
     * @param $product
     * @return string
     */
    public function getReviewsSummaryHtml($product)
    {
        if ($product && $product->getId()) {
            $this->setData('widget_type', WidgetType::SUMMARY_TYPE);
            $this->setData('feedback_product', $product);
            return $this->toHtml();
        }

        return '';
    }

    /**
     * Get product embed widget code
     *
     * @return mixed|string|string[]
     */
    public function getWidgetCode()
    {
        $widgetCode = '';
        $widgetType = $this->getData('widget_type');
        $product = $this->getProduct();

        if ($widgetType && $product) {
            $widgetCode = $this->_widgetFilter->prepareProductCode($widgetType, $product);
        }

        return $widgetCode ? $widgetCode : '';
    }
}
