<?php

namespace FeedbackCompany\Reviews\Block\Widget;

use FeedbackCompany\Reviews\Model\System\Config\Source\WidgetType;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use FeedbackCompany\Reviews\Model\WidgetFilter;
use Magento\Framework\Registry;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;

abstract class AbstractWidget extends Template
{
    /**
     * Set template
     *
     * @var string
     */
    protected $_template = 'widget/widget.phtml';

    /**
     * @var WidgetFilter
     */
    protected $_widgetFilter;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var Config
     */
    protected $_feedbackConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        Template\Context $context,
        WidgetFilter $widgetFilter,
        Registry $registry,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_widgetFilter = $widgetFilter;
        $this->_registry = $registry;
        $this->_feedbackConfig = $config;
        $this->_storeManager = $context->getStoreManager();
    }

    abstract public function getWidgetCode();

    /**
     * Cache lifetime of block
     *
     * @return null
     */
    public function getCacheLifetime()
    {
        return null;
    }

    /**
     * Is active module on store
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isActive()
    {
        return $this->_feedbackConfig->isActive($this->_storeManager->getStore()->getId());
    }

    /**
     * Get current product
     *
     * @return mixed
     */
    public function getProduct()
    {
        if ($this->getData('feedback_product') === null) {
            $this->setData('feedback_product', $this->_registry->registry('current_product'));
        }
        return $this->getData('feedback_product');
    }

    /**
     * Is enable review tab on PDP
     *
     * @return bool
     * @throws LocalizedException
     */
    public function isEnableReviewTab()
    {
        if ($this->getData('widget_type') == WidgetType::SUMMARY_TYPE
            && $this->getLayout()->getBlock('feedback.reviews.tab')) {
            return true;
        }

        return false;
    }
}
