<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Observer\Adminhtml;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use FeedbackCompany\Reviews\Model\Api\Data\AccessToken;

class ConfigState implements ObserverInterface
{
    /**
     * @var AccessToken
     */
    private $accessTokenModel;

    /**
     * @var Config
     */
    private $feedbackConfig;

    public function __construct(AccessToken $accessToken, Config $config)
    {
        $this->accessTokenModel = $accessToken;
        $this->feedbackConfig = $config;
    }

    /**
     * @var bool
     */
    private $shouldUpdateToken = true;

    /**
     * Track changes of system config
     * Create access token if it needs
     *
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $changedPaths = $observer->getEvent()->getChangedPaths();
        $storeId = (int)$observer->getEvent()->getStore() ? (int)$observer->getEvent()->getStore() : 0;

        if (!$this->feedbackConfig->getAccessToken($storeId)) {
            $this->accessTokenModel->registerAccessToken($storeId);
            $this->shouldUpdateToken = false;
        }

        if ($changedPaths) {
            foreach ($changedPaths as $path) {
                switch ($path) {
                    case Config::CLIENT_ID_PASS:
                    case Config::CLIENT_SECRET_PATH:
                        if ($this->shouldUpdateToken && $this->accessTokenModel->registerAccessToken($storeId)) {
                            $this->shouldUpdateToken = false;
                        }
                        break;
                }
            }
        }
    }
}
