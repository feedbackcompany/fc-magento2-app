<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use FeedbackCompany\Reviews\Model\ResourceModel\Widget;

class WidgetTypesByStore implements ObserverInterface
{
    /**
     * @var Widget
     */
    private $widget;

    public function __construct(Widget $widget)
    {
        $this->widget = $widget;
    }

    /**
     * Add widget types for store
     *
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
        $storeId = $observer->getData('store')->getId();
        if ($storeId) {
            $this->widget->insertWidgetTypesByStore([$storeId]);
        }
        return $this;
    }
}
