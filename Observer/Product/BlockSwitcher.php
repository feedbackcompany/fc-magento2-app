<?php
/**
 * Copyright © Feedback Company. All rights reserved.
 */

namespace FeedbackCompany\Reviews\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Layout;
use FeedbackCompany\Reviews\Model\System\Config\Config;
use Magento\Store\Model\StoreManagerInterface;
use FeedbackCompany\Reviews\Model\System\Config\Source\ExtendedWidgetType;

class BlockSwitcher implements ObserverInterface
{
    /**
     * @var Config
     */
    private $feedbackConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(Config $config, StoreManagerInterface $storeManager)
    {
        $this->feedbackConfig = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * Switch blocks on PDP
     * Depends on extended widget type
     *
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** @var Layout $layout */
        $layout = $observer->getEvent()->getLayout();
        $fullActionName = $observer->getEvent()->getFullActionName();
        $storeId = $this->storeManager->getStore()->getId();

        if ($fullActionName == 'catalog_product_view' && $this->feedbackConfig->isActive($storeId)) {
            if ($extendedWidgetType = $this->feedbackConfig->getExtendedWidgetType($storeId)) {
                if ($extendedWidgetType == ExtendedWidgetType::INLINE
                    && $layout->getBlock('feedback.reviews.extended')) {
                    $layout->unsetElement('feedback.reviews.extended');
                }

                if ($extendedWidgetType != ExtendedWidgetType::INLINE
                    && $layout->getBlock('feedback.reviews.tab')) {
                    $layout->unsetElement('feedback.reviews.tab');
                }
            }
        }
    }
}
