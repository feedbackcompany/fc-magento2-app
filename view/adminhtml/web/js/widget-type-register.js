/** Copyright © Feedback Company. All rights reserved. */
define([
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'prototype'
], function ($, uiAlert, $t) {
    'use strict';

    return function (options) {
        var registerButton = $('#registration_button');

        registerButton.click(function () {
            new Ajax.Request(options.ajaxUrl, {
                parameters:     {},
                loaderArea:     false,
                asynchronous:   true,
                onCreate: function() {
                    $('#registration_button').attr('disabled', true);
                    $('#registration-indicator').find('.processing').show();
                },
                onSuccess: function(response) {
                    $('#registration-indicator').find('.processing').hide();

                    if (response.statusText === 'OK' && response.responseJSON.status === 'Successful') {
                        uiAlert({
                            content: $t('You register all widgets successfully.')
                        });
                    } else if (response.responseJSON.status === 'Failed'
                        && response.responseJSON.error_message !== undefined) {
                        uiAlert({
                            content: response.responseJSON.error_message
                        });
                    }
                },
                onComplete: function () {
                    $('#registration_button').attr('disabled', false);
                }
            });
        });
    }
});