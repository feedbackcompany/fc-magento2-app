/** Copyright © Feedback Company. All rights reserved. */
define([
    'jquery'
], function ($) {
    'use strict';

    var widgetTypeElement = $('select[name="parameters[widget_type]"]');
    var widgetImagePreview = $('#widget-image-preview');

    function updatePreviewSource(options) {
        var widgetType = widgetTypeElement.val();

        if (widgetType !== undefined) {
            for (var key in options) {
                if (widgetType === key && options[key] !== '') {
                    widgetImagePreview.attr('src', options[key]);
                }
            }
        }
    }

    return function (options) {
        if (widgetTypeElement.length > 0 && options && options.widgetOptions) {
            updatePreviewSource(options.widgetOptions);

            widgetTypeElement.on('change', function () {
                updatePreviewSource(options.widgetOptions);
            });
        }
    }
});