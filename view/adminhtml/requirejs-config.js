/** Copyright © Feedback Company. All rights reserved. */
var config = {
    map: {
        '*': {
            widgetTypePreview: 'FeedbackCompany_Reviews/js/widget-type-preview',
            registerWidgetTypes: 'FeedbackCompany_Reviews/js/widget-type-register'
        }
    }
};