/** Copyright © Feedback Company. All rights reserved. */
define([
    'jquery'
], function ($) {
    'use strict';

    var reviewTab = $('#tab-label-feedback_reviews');
    var widget = $('.__fbcw_ps__widget-product-summary');

    return function () {
        widget.on('click', function () {
            reviewTab.trigger('click');
            $('html, body').animate({
                scrollTop: reviewTab.offset().top - 50
            }, 300);
        });
    }
});